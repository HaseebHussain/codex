const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//


exports.simpleDbFunction = functions.database.ref('/Inquiries/{ID}')
    .onCreate((snap, context) => {

      const instanceId = snap.val().device_instance;
        const payload = {
          notification : {
              title : "CODEX",
              body : snap.val().message,
              sound : 'default'
          }
      }
      return admin.messaging().sendToDevice(instanceId,payload);

  });


  exports.reserveAlert = functions.database.ref('Reservations/{id}/')
    .onUpdate((change, context) => {
      // Grab the current value of what was written to the Realtime Database.
      // const newValue = change.after.key;
      const values = change.after.val();
      const status = values.status;

      if(status === "approved"){
        const student_id = values.student_id;
var ref = admin.database().ref("registered_users").child("students").orderByChild('student_id').equalTo(student_id);
// Attach an asynchronous callback to read the data at our posts reference
// var query = firebase.database().ref("users").orderByKey();
ref.once("value")
  .then((snapshot) => {
    snapshot.forEach(function(childSnapshot) {
      // key will be "ada" the first time and "alan" the second time
      var key = childSnapshot.key;
      console.log("Key => "+key);
      // childData will be the actual contents of the child
      var childData = childSnapshot.val();
      var ID = childData.device_instance;
          var msg = {
          notification : {
              title : "CODEX",
              body : "You book reservation is approved go get the book",
              sound : 'default'
          }
        }
      admin.messaging().sendToDevice(ID,msg);

  });
  return null;
}).catch(error => {
    console.log("Error : "+e);
});
      }


});

exports.alert = functions.database.ref('Inquiries/{id}')
    .onCreate((snapshot, context) => {
      // Grab the current value of what was written to the Realtime Database.
      const original = snapshot.child("message").val();
      const instanceId = snapshot.child("device_instance").val();

      console.log('message', original);
      console.log('Instance Id', instanceId);
      const payload = {
          notification : {
              title : "CODEX",
              body : original,
              sound : 'default'
          }
      }
      return admin.messaging().sendToDevice(instanceId,payload);
});


exports.bookAlert = functions.database.ref('Book/{id}/')
  .onUpdate((change, context) => {
    // Grab the current value of what was written to the Realtime Database.
    // const newValue = change.after.key;
    const values_after = change.after.val();
    const values_before = change.before.val();

    console.log("Book Updated");

    if(values_after.copies > values_before.copies){
      const book_isbn = values_after.isbn;
      const book_title = values_after.name;



      var ref = admin.database().ref("book_alerts").orderByChild('book_isbn').equalTo(book_isbn);

      ref.once("value")
        .then((snapshot) => {
          snapshot.forEach(function(childSnapshot) {
            // key will be "ada" the first time and "alan" the second time
            var key = childSnapshot.key;
            // childData will be the actual contents of the child
            var childData = childSnapshot.val();

            if(book_isbn === childData.book_isbn){
              var ID = childData.device_instance;
                  const alert_id = childData.alert_id;
              console.log("Device Instance = "+ID);
                  var msg = {
                  notification : {
                      title : "CODEX",
                      body : book_title+" is available now",
                      sound : 'default'
                  }
                }
              admin.messaging().sendToDevice(ID,msg);
              admin.database().ref('book_alerts').child(alert_id).remove();
            }else{
              console.log("No alerts");
            }



        });
        return null;
      }).catch(error => {
          console.log("Error : "+e);
      });

    }


});

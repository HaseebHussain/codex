package com.example.codex;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nex3z.notificationbadge.NotificationBadge;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import de.hdodenhof.circleimageview.CircleImageView;

public class fragmentAdminPanel extends Fragment {

    FirebaseDatabase database;
    DatabaseReference myRef;
    private TextView mReservationRequest,mApprovedBooks,mReserveRequest,mUsers,mIssuedBooks,mIssuesPending;
    CircleImageView mAdminIssueBook;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragement_admin_panel,container,false);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        final NotificationBadge mBadge1 = v.findViewById(R.id.badge1);
        final NotificationBadge mBadge2 = v.findViewById(R.id.badge2);
        final NotificationBadge mBadge3 = v.findViewById(R.id.badge3);
        mReservationRequest = v.findViewById(R.id.ReservationRequest);
        mApprovedBooks = v.findViewById(R.id.ApprovedBooks);
        mAdminIssueBook = v.findViewById(R.id.adminIssueBook);
        mReserveRequest = v.findViewById(R.id.reserve_request);
        mUsers = v.findViewById(R.id.users);
        mIssuedBooks = v.findViewById(R.id.book_issued);
        mIssuesPending = v.findViewById(R.id.issues_pending);

        try{
            myRef.child("Reservations").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    int number = 0;
                    int number2 = 0;
                    if(dataSnapshot.exists()){

                        for(DataSnapshot d: dataSnapshot.getChildren()){
                            String status = d.child("status").getValue(String.class);
                            if(status.equals("pending")){
                                number++;
                            }else if(status.equals("approved")){
                                number2++;
                            }
                        }
                        if(number == 0){
                            mReserveRequest.setText("0");
//                            mReservationRequest.setText("Not available");
//                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
//                                // Do something for lollipop and above versions
////                                mReservationRequest.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
//                            } else{
//                                // do something for phones running an SDK before lollipop
////                                mReservationRequest.setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
//                            }

                        }else{
//                            mBadge1.setNumber(number);
                            mReserveRequest.setText(""+number);
                        }
                        if(number2 == 0){
                            mIssuesPending.setText(""+number2);
                        }else{
                            mIssuesPending.setText(""+number2);
                        }
                    }else{
                        mReserveRequest.setText("0");
                        mIssuesPending.setText("0");
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

            DatabaseReference issueBooks = myRef.child("issued_books");
           if(issueBooks.getPath() != null){
               issueBooks.addValueEventListener(new ValueEventListener() {
                   @Override
                   public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                       int number = 0;
                       if(dataSnapshot.exists()){

                           for(DataSnapshot d: dataSnapshot.getChildren()){
                               number++;

                           }
                            mIssuedBooks.setText(""+number);
                       }else{
                           mIssuedBooks.setText(""+number);
                       }
                   }

                   @Override
                   public void onCancelled(@NonNull DatabaseError databaseError) {

                   }
               });
           }


//        Calculate Users
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("registered_users");
           userRef.child("students").addValueEventListener(new ValueEventListener() {
               @Override
               public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                   int number = 0;
                    if(dataSnapshot.exists()){
                        for (DataSnapshot d : dataSnapshot.getChildren()){
                            number++;
                        }
                        mUsers.setText(""+number);
                    }else{
                        mUsers.setText(""+number);
                    }
               }

               @Override
               public void onCancelled(@NonNull DatabaseError databaseError) {

               }
           });

//           admin issue a book
        mAdminIssueBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(),Activity_admin_issue_book.class);
                startActivity(intent);
            }
        });

        return v;
    }
}

package com.example.codex;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class user_inquery extends AppCompatActivity {

    private static final String TAG = "TAG";
    TextView mTxtMessage;
    String deviceInstance,name,user_id;
    ArrayList<Model_Inquery> user_messages;
    DatabaseReference myRef;
    Date currentTime;
    ListView rv_InquireMesssage;
    ProgressDialog progressDialog;
    Button mBtnSendMessage;
    String userID;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_inquery);

        preferences = getSharedPreferences("codex",MODE_PRIVATE);
        name = PreferenceManager.getDefaultSharedPreferences(this).getString("Username","Not Found");
        user_id = PreferenceManager.getDefaultSharedPreferences(this).getString("User_ID","Not Found");

        userID = preferences.getString("university_id","null");

        mTxtMessage = findViewById(R.id.txt_message);
        myRef = FirebaseDatabase.getInstance().getReference("Inquiries");
        rv_InquireMesssage = findViewById(R.id.user_messages);
        mBtnSendMessage = findViewById(R.id.btnSendMessage);
        user_messages = new ArrayList<>();

//        Progress Dailog
        progressDialog = new ProgressDialog(user_inquery.this);
        progressDialog.setMessage("Loading Conversations...");
        progressDialog.show();

        Query q = myRef.orderByChild("chat_id").equalTo(userID);
        q.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                user_messages.clear();
                for(DataSnapshot d : dataSnapshot.getChildren()){
                    Model_Inquery m = d.getValue(Model_Inquery.class);
                    user_messages.add(m);
                }


                AdaptorUserInqueryMessages adaptorUserInqueryMessages = new AdaptorUserInqueryMessages(user_inquery.this,user_messages);
                rv_InquireMesssage.setAdapter(adaptorUserInqueryMessages);
                LinearLayoutManager mLayoutManager = new LinearLayoutManager(user_inquery.this);
                mLayoutManager.setStackFromEnd(true);
// Set the layout manager to your recyclerview
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(user_inquery.this, "Chat Loading error", Toast.LENGTH_SHORT).show();
            }
        });





    }

    public void sendMessage(View view){
//        String id = myRef.push().getKey();
//        Calendar c = Calendar.getInstance();


            // Name, email address, and profile photo Url
progressDialog.setMessage("Sending message...");
progressDialog.show();

            DateFormat df = new SimpleDateFormat("dd/MMM h:mm aa");
            Date date = new Date();
            final String formattedDate = df.format(date);

            final String messageId = myRef.push().getKey();

            final DatabaseReference myRef2 = FirebaseDatabase.getInstance().getReference("admin");
            myRef2.child("device_instance").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    deviceInstance = dataSnapshot.getValue(String.class);
                    Model_Inquery i = new Model_Inquery(messageId,userID,mTxtMessage.getText().toString(),deviceInstance,formattedDate,"Codex",false,userID);
                myRef.child(messageId).setValue(i)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            progressDialog.dismiss();
                            Toast.makeText(user_inquery.this, "Message Sent", Toast.LENGTH_SHORT).show();
                            mTxtMessage.setText("");

                        }
                    });



                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });




    }

    public int gen() {
        Random r = new Random( System.currentTimeMillis() );
        return 10000 + r.nextInt(20000);
    }
}

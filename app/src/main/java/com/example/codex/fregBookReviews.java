package com.example.codex;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.content.Context.MODE_PRIVATE;

public class fregBookReviews extends Fragment {

    FloatingActionButton mAddReview;
    DatabaseReference mRef;
    ProgressDialog progressDialog;
    private String student_id;
    private String isbn,category;
    List<Model_Book_Review> ReviewList;
    private RecyclerView mRecycleView;
    SharedPreferences preferences;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.freg_book_reviews,container,false);

      mRef = FirebaseDatabase.getInstance().getReference("reviews");
      mAddReview = v.findViewById(R.id.addReview);
      progressDialog = new ProgressDialog(getContext());
      ReviewList = new ArrayList<>();
      mRecycleView = v.findViewById(R.id.ReviewList);
      preferences = getActivity().getSharedPreferences("codex",MODE_PRIVATE);

      student_id =  preferences.getString("university_id", "Null");
      isbn = getActivity().getIntent().getExtras().getString("isbn");
      category = preferences.getString("category","null");
      if(category.equals("admin")){
          mAddReview.setVisibility(View.GONE);
      }


      mAddReview.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              final Dialog reviewDialog = new Dialog(getContext());
              reviewDialog.setContentView(R.layout.layout_add_review);
              reviewDialog.setTitle("Add Review");

              final Spinner spinner = (Spinner) reviewDialog.findViewById(R.id.rate);
              final TextView mMessage = reviewDialog.findViewById(R.id.message);
              MaterialButton mAddReview = reviewDialog.findViewById(R.id.addReview);
              MaterialButton mClose = reviewDialog.findViewById(R.id.btnClose);

              // Spinner Drop down elements
              List<Integer> rating = new ArrayList<>();
              rating.add(1);
              rating.add(2);
              rating.add(3);
              rating.add(4);
              rating.add(5);

              ArrayAdapter<Integer> dataAdapter = new ArrayAdapter<>(getContext(),android.R.layout.simple_list_item_1,rating);
              dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
              spinner.setAdapter(dataAdapter);

//              Add review
              mAddReview.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                      progressDialog.setTitle("Please Wait...!");
                      String id = mRef.push().getKey();


                     String comment = mMessage.getText().toString();
                      Model_Book_Review review = new Model_Book_Review(id,student_id,isbn,comment, (Integer) spinner.getSelectedItem());

                      mRef.child(id).setValue(review).addOnCompleteListener(new OnCompleteListener<Void>() {
                          @Override
                          public void onComplete(@NonNull Task<Void> task) {

                              Toast.makeText(getContext(), "Review added successfully!", Toast.LENGTH_SHORT).show();

                                reviewDialog.dismiss();


                          }
                      });


                  }
              });

//              Close dialog
              mClose.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                      reviewDialog.dismiss();
                  }
              });




              reviewDialog.show();
          }
      });


//      checking if user already give review
        Query query1 = mRef.orderByChild("user_id").equalTo(student_id);
        query1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

               if(dataSnapshot.exists()){
                  for (DataSnapshot d: dataSnapshot.getChildren()){
                      Model_Book_Review review = d.getValue(Model_Book_Review.class);
                      String getisbn = review.getBook_isbn();
                      String getuserId = review.getUser_id();
                      if(getisbn.equals(isbn) && getuserId.equals(student_id)){
                          mAddReview.setVisibility(View.GONE);
                      }
                  }
               }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

//        Load Book Reviews
        Query query2 = mRef.orderByChild("book_isbn").equalTo(isbn);
        query2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ReviewList.clear();
                if(dataSnapshot.exists()){
                    double average = 0;
                    double count = 0;
                    double rate;
                    for(DataSnapshot d: dataSnapshot.getChildren()){
                        Model_Book_Review review = d.getValue(Model_Book_Review.class);
                        ReviewList.add(review);
                        average = (double)review.getRating();
                        count++;
                    }
                    rate = average/count;
                    updateBookRate(isbn,rate);
                    Adapter_Book_Reviews adapter = new Adapter_Book_Reviews(ReviewList,getContext());
                    mRecycleView.setAdapter(adapter);
                    LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
                    mRecycleView.setLayoutManager(manager);


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return v;
    }

    private void updateBookRate(String isbn, double rate) {
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("Book").child("-H"+isbn);
        myRef.child("rating").setValue(rate).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                progressDialog.dismiss();
            }
        });
    }
}

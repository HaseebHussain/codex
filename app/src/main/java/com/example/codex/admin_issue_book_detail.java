package com.example.codex;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class admin_issue_book_detail extends AppCompatActivity {

    ImageView mBookImage;
    TextView mBookTitle,mWriter;
    AutoCompleteTextView mMemberId;
    List<String> mList_IDs;
    List<String> mStd_IDs;
    String isbn;
    int book_copies;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_issue_book_detail);

        mBookImage = findViewById(R.id.book_image);
        mBookTitle = findViewById(R.id.book_title);
        mWriter = findViewById(R.id.book_author);
        mMemberId = findViewById(R.id.member_id);
         isbn = getIntent().getExtras().getString("isbn");
        mBookTitle.setText(isbn);
        mList_IDs = new ArrayList<>();
        mStd_IDs = new ArrayList<>();

//        Ceating list for autocomplete
        DatabaseReference myRef1 = FirebaseDatabase.getInstance().getReference("registered_users").child("students");
        myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    for (DataSnapshot d : dataSnapshot.getChildren()){
                        Model_Student_Detail detail = d.getValue(Model_Student_Detail.class);
                        String uni_id = detail.getUniversity_id();
                        mList_IDs.add(uni_id);
                        mStd_IDs.add(detail.getStudent_id());

                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(admin_issue_book_detail.this,android.R.layout.simple_list_item_1,mList_IDs);
                    mMemberId.setAdapter(adapter);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("Book").child("-H"+isbn);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    try{
                        String image = dataSnapshot.child("imageUri").getValue(String.class);
                        String title = dataSnapshot.child("name").getValue(String.class);
                        if(!image.equals(null) && !image.equals("")){
                            Picasso.get().load(image).into(mBookImage);
                        }
                        mBookTitle.setText(title);
                        mWriter.setText(dataSnapshot.child("writer").getValue(String.class));
                        book_copies = dataSnapshot.child("copies").getValue(Integer.class);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Toast.makeText(admin_issue_book_detail.this, "Book is not found", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(admin_issue_book_detail.this,Activity_admin_issue_book.class);
                    startActivity(i);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void IssueBook(View view) {

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH)+6);
        Date date = cal.getTime();
        String returnDate = df.format(date);

        String currentDate = new ConnectionCheck(this).CurrentDate();
        String currentTime = new ConnectionCheck(this).CurrentTime();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("issued_books");

        final String id = ref.push().getKey();

        String university_id = mMemberId.getText().toString();
        String student_id = "";
        for(int i = 0; i < mList_IDs.size(); i++)
        {
            if(mList_IDs.contains(university_id))
            {
                student_id = mStd_IDs.get(i);
            }
        }


        Model_Book_Issue data = new Model_Book_Issue(id,isbn,student_id,currentDate,returnDate,currentTime,"issued");
        ref.child(id).setValue(data).addOnCompleteListener(new OnCompleteListener<Void>() {

            @Override
            public void onComplete(@NonNull Task<Void> task) {
                DatabaseReference BookRef = FirebaseDatabase.getInstance().getReference("Book").child("-H"+isbn);
                BookRef.child("copies").setValue((book_copies-1)).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(admin_issue_book_detail.this, "Book Issued Successfully", Toast.LENGTH_SHORT).show();
                        mMemberId.setText("");
                    }
                });

            }
        });
    }
}

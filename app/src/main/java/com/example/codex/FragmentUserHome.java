package com.example.codex;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentUserHome extends Fragment {



    private List<Model_Book_Details> mFeatured_books_list;
    RecyclerView recyclerView;

    private ActionBarDrawerToggle t;

    String[] department_names = new String[]{
            "Law","English","Sociology","Psychology",
            "Computer Science", "Electronics","Sindhi",
            "Islamic Studies","Mathematics","Statistics",
            "Physics","Chemistry","Pharmacy","Management",
            "Accounting","Economics","Finance","Business",
            "Banking","Pakistan Studies","Politics","Science",
            "Urdu"
    };


    int[] images = new int[]{
            R.drawable.libra,
            R.drawable.english,
            R.drawable.sociology,
            R.drawable.idea,
            R.drawable.ebook,
            R.drawable.electronics,
            R.drawable.sindhi,
            R.drawable.quran,
            R.drawable.mathematics,
            R.drawable.statistics,
            R.drawable.physics,
            R.drawable.chemistry,
            R.drawable.pills,
            R.drawable.management,
            R.drawable.accounting,
            R.drawable.economics,
            R.drawable.finance,
            R.drawable.bisuness,
            R.drawable.banking,
            R.drawable.mosque,
            R.drawable.politics,
            R.drawable.science,
            R.drawable.urdu
    };

    String[] department_description = new String[]{
            "Find books related to Law",
            " Find books related to English",
            "Find books related to Sociology",
            "Find books related to Psychology",
            "Find books related to Computer Science",
            "Find books related to Electronics",
            "Find books related to Sindhi",
            "Find books related to Islamic Studies",
            "Find books related to Mathematics",
            "Find books related to Statistics",
            "Find books related to Physics",
            "Find books related to Chemistry",
            "Find books related to Pharmacy",
            "Find books related to Management",
            "Find books related to Accounting",
            "Find books related to Economics",
            "Find books related to Finance",
            "Find books related to Business",
            "Find books related to Banking",
            "Find books related to Pakistan Studies",
            "Find books related to Politics",
            "Find books related to Science",
            "Find books related to Urdu"
    };

    private ImageView imageView;
    private String studentID;
    CircleImageView imgProfileImage;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =inflater.inflate(R.layout.fragment_user_home, container, false);

        mFeatured_books_list = new ArrayList<>();
        DatabaseReference mDatabaseRef = FirebaseDatabase.getInstance().getReference("Book");
        Query q = FirebaseDatabase.getInstance().getReference("Book").orderByChild("rating").limitToFirst(6);
        recyclerView = v.findViewById(R.id.featured_list);

//        mBtn_logout = findViewById(R.id.btn_logout);
        String admin = getActivity().getIntent().getStringExtra("admin");
        if(!TextUtils.isEmpty(admin)){
//            mBtn_logout.setVisibility(View.GONE);
        }

        ConnectionCheck cc = new ConnectionCheck(getContext());

        boolean connection = cc.RequestInternetConnection();



        if(connection) {
            q.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    mFeatured_books_list.clear();

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                        Model_Book_Details upload = postSnapshot.getValue(Model_Book_Details.class);
                        mFeatured_books_list.add(upload);

                    }

                    Adaptor_Featured_Books adaptor_featured_books = new Adaptor_Featured_Books(getContext(),mFeatured_books_list);



                    LinearLayoutManager layoutManager = new
                            LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                    layoutManager.setReverseLayout(true);
                    layoutManager.setStackFromEnd(true);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(adaptor_featured_books);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        } else{
            Toast.makeText(getContext(), "Internet is not connected", Toast.LENGTH_SHORT).show();
        }


        String token = FirebaseInstanceId.getInstance().getToken();
        loadProfileImage();


        List<HashMap<String, String>> aList = new ArrayList<>();

        for (int i = 0; i < department_names.length; i++) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("depart_title", department_names[i]);
            hm.put("depart_description", department_description[i]);
            hm.put("depart_images", Integer.toString(images[i]));
            aList.add(hm);
        }

        String[] from = {"depart_images", "depart_title", "depart_description"};
        int[] to = {R.id.listview_image, R.id.listview_item_title, R.id.listview_item_short_description};

        final SimpleAdapter simpleAdapter = new SimpleAdapter(getContext(), aList, R.layout.layout_department_list, from, to);
        final ListView androidListView = v.findViewById(R.id.list_depertment);
        androidListView.setAdapter(simpleAdapter);

        androidListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object o = androidListView.getItemAtPosition(position);
                HashMap<String,String> s = (HashMap<String, String>) o;

                Intent intent = new Intent(getContext(),bookCategories.class);
                intent.putExtra("department",s.get("depart_title"));
                int res = Integer.parseInt(s.get("depart_images"));
                intent.putExtra("image",res);
                startActivity(intent);

            }
        });

        studentID = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("User_ID","Null");


        return v;
    }

    public void loadProfileImage(){
        //        Getting user image
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("Registered_User");
        Query query = myRef.orderByChild("studentid").equalTo(studentID);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot d: dataSnapshot.getChildren()){
                    String profileImage = d.child("profileImage").getValue(String.class);
                    Picasso.get().load(profileImage).noFade().into(imgProfileImage);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}

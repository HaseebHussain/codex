package com.example.codex;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.util.zip.Inflater;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContentResolverCompat;
import androidx.fragment.app.Fragment;

import static android.app.Activity.RESULT_OK;

public class FragmentBookEdit extends Fragment {

    ConstraintLayout mLayout;
    TextView mTextView_Title,mTextView_Writer,mTextView_Edition,mTextView_Status,mTextView_Copies,mTvDescription,mPdf;
    ImageView mImg_Book,mEditCopies,mEditTitle,mPdfEdit;
    FloatingActionButton mBtnAlert;
    com.github.clans.fab.FloatingActionMenu mBtnAdminMenu;
    com.github.clans.fab.FloatingActionButton mBtnEdit,mBtnDelete;
    private DatabaseReference mDatabase;
    Button mBtn_Reserve_Book,mBtnChangeImage,mPdfUpdate;
    String name,status,isbn;
    DatabaseReference mDbRef;
    String Date;
    String instance ;
    String User;
    private ImageView mImage;
    private StorageReference mStorage;
    private SharedPreferences preferences;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_book_edit,container,false);
        mImg_Book = v.findViewById(R.id.img_book);
        mTextView_Title = v.findViewById(R.id.tv_title);
        mTextView_Writer = v.findViewById(R.id.tv_writer);
        mTextView_Edition = v.findViewById(R.id.tv_edition);
        mTextView_Status  = v.findViewById(R.id.tv_status);
        mTextView_Copies = v.findViewById(R.id.tv_copies);
        mTvDescription = v.findViewById(R.id.tvDescription);
        mEditCopies = v.findViewById(R.id.editCopies);
        mBtnChangeImage = v.findViewById(R.id.btnChangeBookImage);
        mEditTitle = v.findViewById(R.id.editTitle);
        mLayout = v.findViewById(R.id.layout);
        mPdfEdit = v.findViewById(R.id.edit_pdf);
        mPdf = v.findViewById(R.id.pdf);

        preferences = getActivity().getSharedPreferences("codex", Context.MODE_PRIVATE);

        User = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("category","Not Found");

        if(User.equals("user")){
            mBtnAdminMenu.setVisibility(View.GONE);
        }else{

        }



        mDbRef = FirebaseDatabase.getInstance().getReference("Book");


        //        firbase
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Reservations");
        isbn = getActivity().getIntent().getStringExtra("isbn");
        Log.i("TAG", "ISBN : "+isbn);
        Query book_detail = mDbRef.orderByChild("isbn").equalTo(isbn);
        book_detail.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    for(DataSnapshot d : dataSnapshot.getChildren()){
                        Model_Book_Details model_book_details = d.getValue(Model_Book_Details.class);
                        mTextView_Title.setText(model_book_details.getName());
                        mTextView_Writer.setText(model_book_details.getWriter());
                        mTextView_Edition.setText(model_book_details.getEdition());
                        mTextView_Status.setText(model_book_details.getStatus());
                        mTextView_Copies.setText(String.valueOf(model_book_details.getCopies()));
                        Picasso.get()
                                .load(model_book_details.getImageUri())
                                .placeholder(R.mipmap.ic_launcher)
                                .into(mImg_Book);
                        mTvDescription.setText(model_book_details.getDescription());
                        mPdf.setText(model_book_details.getPdf());
                    }
                }else{
                    Toast.makeText(getContext(), "Data Not found", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        mEditCopies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getContext());
                dialog.setContentView(R.layout.layout_copies_edit);

                TextView mTvTitle = dialog.findViewById(R.id.tvTitle);
                final EditText mEdValue = dialog.findViewById(R.id.etValue);
                Button mBtnUpdate = dialog.findViewById(R.id.btnUpdate);

                mTvTitle.setText("No. of Copies");
                mEdValue.setText(mTextView_Copies.getText());
                mBtnUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int copies = Integer.parseInt(mEdValue.getText().toString());
                 DatabaseReference ref =  FirebaseDatabase.getInstance().getReference("Book").child("-H"+isbn);
                         ref.child("copies").setValue(copies);

                         if(copies == 0){
                             ref.child("status").setValue("Not Available");
                         }else{
                             ref.child("status").setValue("Available");
                         }

                      dialog.dismiss();
                        Snackbar.make(v,"Book Updated",Snackbar.LENGTH_SHORT).show();


                    }
                });
              dialog.show();



            }
        });


        mEditTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getContext());
                dialog.setContentView(R.layout.layout_copies_edit);

                TextView mTvTitle = dialog.findViewById(R.id.tvTitle);
                final EditText mEdValue = dialog.findViewById(R.id.etValue);
                Button mBtnUpdate = dialog.findViewById(R.id.btnUpdate);

                mTvTitle.setText("Book Title");
                mEdValue.setText(mTextView_Title.getText());
                mEdValue.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.MATCH_PARENT;

                mBtnUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String copies = mEdValue.getText().toString();
                        DatabaseReference ref =  FirebaseDatabase.getInstance().getReference("Book").child("-H"+isbn);
                        ref.child("name").setValue(copies);
                        dialog.dismiss();
                        Snackbar.make(mLayout,"Book Updated",Snackbar.LENGTH_SHORT).show();


                    }
                });
                dialog.show();
            }
        });


        mBtnChangeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(requsetPermissoin()){
                    OpenGallery();
                }
            }
        });

//       update pdf
        mPdfEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getContext());
                dialog.setContentView(R.layout.layout_edit_book_pdf);
                final ProgressDialog progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage("Please Wait...");

                final TextView mPdf = dialog.findViewById(R.id.pdf);
                Button mBtnUpdate = dialog.findViewById(R.id.btnUpdate);
                mPdfUpdate = dialog.findViewById(R.id.update);


                mBtnUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        DatabaseReference ref =  FirebaseDatabase.getInstance().getReference("Book").child("-H"+isbn);
//                        ref.child("name").setValue("");
//                        dialog.dismiss();
//                        Snackbar.make(mLayout,"Book Updated",Snackbar.LENGTH_SHORT).show();

//                        Intent intent = new Intent();
//                        startActivityForResult(intent,2);
//                        intent.setType("application/pdf");
//                        startActivity(intent);
                        Intent browseStorage = new Intent(Intent.ACTION_GET_CONTENT);
                        browseStorage.setType("application/pdf");
//                        browseStorage.addCategory(Intent.CATEGORY_OPENABLE);
                        startActivityForResult(Intent.createChooser(browseStorage, "Select PDF"), 2);

                    }
                });

                mPdfUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        progressDialog.show();
                        String pdf_uri = preferences.getString("pdf_uri","null");
                        try {
                            if(pdf_uri != "null"){
                               final StorageReference ref = FirebaseStorage.getInstance().getReference("books_pdf/");
                               ref.child(isbn).putFile(Uri.parse(pdf_uri)).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                   @Override
                                   public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                                       ref.child(isbn).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                           @Override
                                           public void onSuccess(Uri uri) {
                                                DatabaseReference bookRef = FirebaseDatabase.getInstance().getReference("Book").child("-H"+isbn);
                                                bookRef.child("pdf").setValue(uri.toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        preferences.edit().remove("pdf_uri").apply();
                                                        progressDialog.dismiss();
                                                        dialog.dismiss();
                                                        Toast.makeText(getContext(), "Book Pdf Added", Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                           }
                                       });
                                   }
                               });
                            }else{
                                progressDialog.dismiss();
                            }
                        }catch (Exception e){
                            progressDialog.dismiss();
                            Toast.makeText(getContext(), e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                dialog.show();
            }
        });
//        btn update




        return v;
    }



    private boolean requsetPermissoin(){
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getContext(), "Permission Already Granted", Toast.LENGTH_SHORT).show();
            return true;
        }
        else{
            ActivityCompat.requestPermissions(getActivity(),new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},1);
            return false;
        }
        // Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //startActivityForResult(intent,0);


    }


    private void OpenGallery(){
//        ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.ACCESS_NETWORK_STATE},0);
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(Intent.createChooser(galleryIntent,"Select Image from Gallery"),1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        try {
            // When an Image is picked
            if (requestCode == 1 && resultCode == RESULT_OK) {
                {
                    Uri uri = data.getData();
                    mStorage = FirebaseStorage.getInstance().getReference("Book_Images");
                    mStorage.child(isbn).putFile(uri);
                    mStorage.child(isbn).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                String Imageuri = uri.toString();
                                DatabaseReference ref =  FirebaseDatabase.getInstance().getReference("Book");
                                ref.child("-H"+isbn).child("imageUri").setValue(Imageuri);
                                Log.i("TAG", "Book URI => "+Imageuri);
                            }
                        });

                }

            }else if (requestCode == 2 && resultCode == RESULT_OK){
                // Get the Uri of the selected file
                Uri uri = data.getData();
                String uriString = uri.toString();
                File myFile = new File(uriString);
                String path = myFile.getAbsolutePath();

                preferences.edit().putString("pdf_uri",uriString).apply();

                    try {

                        Toast.makeText(getContext(), path, Toast.LENGTH_SHORT).show();
                    } catch (Exception e){
                        Toast.makeText(getContext(), e.toString(), Toast.LENGTH_SHORT).show();
                    }
            }else
            {
                Toast.makeText(getActivity(), "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        } catch(Exception e){
            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG)
                    .show();

        }
    }
}

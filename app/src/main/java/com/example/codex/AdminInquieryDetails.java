package com.example.codex;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class AdminInquieryDetails extends AppCompatActivity {

    ArrayList<Model_Inquery> list;
    ListView listView;
    DatabaseReference myRef;
    EditText mTxtMessage;
    static String name,user_id,deviceInstance;
    SharedPreferences preferences;
    ProgressDialog waiting;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_inquiery_details);

        preferences = getSharedPreferences("codex",MODE_PRIVATE);

        waiting = new ProgressDialog(this);
        waiting.setMessage("Sending message...");

        listView = findViewById(R.id.list);
        list = new ArrayList<>();
        mTxtMessage = findViewById(R.id.txt_message);
        deviceInstance = FirebaseInstanceId.getInstance().getToken();
        name = getIntent().getExtras().getString("name");
        myRef = FirebaseDatabase.getInstance().getReference("Inquiries");
        Query q = myRef.orderByChild("chat_id").equalTo(name);
        q.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                list.clear();
                for(DataSnapshot d : dataSnapshot.getChildren()){
                    Model_Inquery m = d.getValue(Model_Inquery.class);
                    list.add(m);

                    boolean read = m.isRead();
                    String message_id = m.getMessage_id();
                    if(!read){

                        myRef.child(message_id).child("read").setValue(true);

                    }
                }

                AdaptorAdminUserInqueries userInqueries = new AdaptorAdminUserInqueries(AdminInquieryDetails.this,list);
                listView.setAdapter(userInqueries);
            }

            @Override
            public void onCancelled(@NotNull DatabaseError databaseError) {

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(AdminInquieryDetails.this, list.get(i).getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void sendMessage(View view) {
        final String messageId = myRef.push().getKey();
        Calendar c = Calendar.getInstance();

        DateFormat df = new SimpleDateFormat("dd/MMM h:mm aa");
        Date date = new Date();
        final String formattedDate = df.format(date);
        waiting.show();

        Query query = FirebaseDatabase.getInstance().getReference("registered_users").child("students");
        query.orderByChild("university_id").equalTo(name)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        try{
                            if(dataSnapshot.exists()){
                                for(DataSnapshot d : dataSnapshot.getChildren()){
                                    String device_instance2 = d.child("device_instance").getValue(String.class);
                                String msg = mTxtMessage.getText().toString();
                                Model_Inquery i = new Model_Inquery(messageId,"Codex",msg,device_instance2,formattedDate,name,false,name);
                                myRef.child(messageId).setValue(i)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                Toast.makeText(AdminInquieryDetails.this, "Message Sent", Toast.LENGTH_SHORT).show();
                                                 mTxtMessage.setText("");
                                                 waiting.dismiss();
                                            }
                                        });
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });



    }


    public int gen() {
        Random r = new Random( System.currentTimeMillis() );
        return 10000 + r.nextInt(20000);
    }
}

package com.example.codex;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class Add_New_Book extends AppCompatActivity {

    private static final int CAMERA_REQUEST_CODE = 1;

    Button mbtn_addbook,mBtn_Camera;
    DatabaseReference reference;
    ProgressBar process;
    ImageView mImage;
    StorageReference mStorage;
    Spinner msp_department;
    EditText mEd_Name,mEd_Writer,mEd_Edition,mEd_Status,mEd_Copies;
    ProgressDialog progressDialog;
    String Imageuri;
    Uri uri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__new__book);
        init();

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.department, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        msp_department.setAdapter(adapter);

        mbtn_addbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addbook();
            }
        });

        //onClick listener for Opening Camera
        mBtn_Camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(requsetPermissoin()){

                    OpenGallery();
                }

            }
        });


    }

    private void init(){
        mbtn_addbook = findViewById(R.id.btn_addBook);

        process = findViewById(R.id.process);
        mBtn_Camera = findViewById(R.id.btn_openGallery);
        mImage = findViewById(R.id.imageView);
        mStorage = FirebaseStorage.getInstance().getReference("Book_Images");
        msp_department = findViewById(R.id.spinner);
        mEd_Name = findViewById(R.id.Ed_Name);
        mEd_Writer = findViewById(R.id.Ed_Writer);
        mEd_Edition = findViewById(R.id.Ed_Edition);
        mEd_Status = findViewById(R.id.Ed_Staus);
        mEd_Copies = findViewById(R.id.Ed_Copies);
        progressDialog = new ProgressDialog(this);

    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == CAMERA_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                {
                    uri = data.getData();

                    try {

                        if(!TextUtils.isEmpty(mEd_Name.getText())){
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                            mImage.setImageBitmap(bitmap);
                            StorageReference sp = mStorage.child(mEd_Name.getText().toString());
                            sp.putFile(uri);
                            sp.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    Imageuri = uri.toString();
                                    Toast.makeText(Add_New_Book.this, "Image Selected Successfully", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                        else {
                            Toast.makeText(this, "Please Enter Book Name", Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

            }else
            {
                Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        } catch(Exception e){
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();

        }
    }

    private boolean requsetPermissoin(){
        if (ActivityCompat.checkSelfPermission(Add_New_Book.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Permission Already Granted", Toast.LENGTH_SHORT).show();
            return true;
        }
        else{
            ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},CAMERA_REQUEST_CODE);
            return false;
        }
        // Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //startActivityForResult(intent,0);


    }

    private void OpenGallery(){
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.ACCESS_NETWORK_STATE},0);
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(Intent.createChooser(galleryIntent,"Select Image from Gallery"),CAMERA_REQUEST_CODE);
    }

    public void addbook() {

        progressDialog.setMessage("Add Book Details");
        progressDialog.show();

        String name = mEd_Name.getText().toString();
        String writer = mEd_Writer.getText().toString();
        String edition = mEd_Edition.getText().toString();
        String status = mEd_Status.getText().toString();
        int copies = Integer.parseInt(mEd_Copies.getText().toString());
        String depart = msp_department.getSelectedItem().toString();
        Toast.makeText(this, depart, Toast.LENGTH_SHORT).show();

//        Model_Book_Details newbook = new Model_Book_Details("","","","","",0,"","","","","","","","","","","","",false);
//        FirebaseDatabase database = FirebaseDatabase.getInstance();
//        DatabaseReference myRef = database.getReference("Book");
//        String id = myRef.push().getKey();
//        myRef.child(id).setValue(newbook, new DatabaseReference.CompletionListener() {
//            @Override
//            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
//                if (databaseError == null) {
//                    Toast.makeText(Add_New_Book.this, "Book added Successfully", Toast.LENGTH_SHORT).show();
//                    progressDialog.dismiss();
//                } else {
//                    Toast.makeText(Add_New_Book.this, "Something went wrong", Toast.LENGTH_SHORT).show();
//                    process.setVisibility(View.GONE);
//                    progressDialog.dismiss();
//                }
//            }
//        });
    }
}


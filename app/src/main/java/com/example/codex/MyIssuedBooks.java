package com.example.codex;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyIssuedBooks extends Fragment {

    private GridView mGridView;
    List<Model_Book_Issue> BookList;
    SharedPreferences preferences;
    ProgressDialog progressDialog;

    public MyIssuedBooks() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_my_issued_books, container, false);

        mGridView = v.findViewById(R.id.gridIssueBooks);
        BookList = new ArrayList<>();

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading Information ... ");
        progressDialog.show();
        preferences = getContext().getSharedPreferences("codex", Context.MODE_PRIVATE);

        String student_id = preferences.getString("student_id","null");
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("issued_books");

        Query q = ref.orderByChild("student_id").equalTo(student_id);
        q.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                BookList.clear();
                if(dataSnapshot.exists()){
                    for(DataSnapshot d : dataSnapshot.getChildren()){

                        Model_Book_Issue issue = d.getValue(Model_Book_Issue.class);
                        try{
                            if(issue.getStatus() != null){
                                if(issue.getStatus().equals("issued")){
                                    BookList.add(issue);
                                }
                            }
                        }catch (Exception e){
                            Toast.makeText(getContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                    }

                    Adapter_book_issue adapter = new Adapter_book_issue(BookList,getActivity());
                    mGridView.setAdapter(adapter);
                    progressDialog.dismiss();
                }else{
                    Toast.makeText(getContext(), "No Issued Book Found", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return v;
    }

}

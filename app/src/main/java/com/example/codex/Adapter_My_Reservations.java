package com.example.codex;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class Adapter_My_Reservations extends RecyclerView.Adapter<Adapter_My_Reservations.ViewHolder>  {


    private Context mContext;
    private List<Model_Reserve_Book> mList;

    public Adapter_My_Reservations(Context mContext, List<Model_Reserve_Book> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public Adapter_My_Reservations.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.layout_my_reservations, parent, false);
        return new Adapter_My_Reservations.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final Adapter_My_Reservations.ViewHolder holder, int position) {

        final Model_Reserve_Book book = mList.get(position);

        holder.mTvBookTitle.setText(book.getBook_title());
        holder.mDate.setText(book.getDate());
        holder.mRequestStatus.setText(book.getStatus());
        Picasso.get().load(book.getBook_image()).into(holder.mImgBookImage);

        holder.mBtnCancelRequest.setSelected(true);

        if(holder.mRequestStatus.getText().equals("approved")){
            holder.mBtnCancelRequest.setEnabled(false);

        }

        holder.mImgBookImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "Cancel Book", Toast.LENGTH_SHORT).show();
            }
        });

        holder.mBtnCancelRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             String revID = book.getReservation_id();
                DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("Reservations");
                myRef.child(revID).removeValue();
            }
        });





    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTvStudentName,mTvBookTitle,mRequestStatus,mDate;
        MaterialButton mBtnCancelRequest;
        ImageView mImgBookImage;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mTvBookTitle = itemView.findViewById(R.id.tv_book_title);
            mRequestStatus = itemView.findViewById(R.id.tv_status);
            mImgBookImage = itemView.findViewById(R.id.img_book_image);
            mDate = itemView.findViewById(R.id.tv_request_date);
            mBtnCancelRequest = itemView.findViewById(R.id.btnCancelRequest);

        }
    }
}

package com.example.codex;


import android.app.DatePickerDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class Adaptor_Reservations  extends RecyclerView.Adapter<Adaptor_Reservations.ViewHolder> {

    private Context mContext;
    private List<Model_Reserve_Book> mList;



    public Adaptor_Reservations(Context mContext, List<Model_Reserve_Book> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView mTvBookTitle, mTvDate,mBookIsbn,mStudentId;
        ImageView mBookImage;
        MaterialButton mBtnApprove, mBtnNotApprove;


        public ViewHolder(View itemView) {
            super(itemView);
            mBookIsbn = itemView.findViewById(R.id.BookIsbn);
            mStudentId = itemView.findViewById(R.id.StudentId);
            mTvBookTitle = itemView.findViewById(R.id.tvBookTitle);
            mBookImage = itemView.findViewById(R.id.BookImage);
            mBtnApprove = itemView.findViewById(R.id.btn_approve);
            mBtnNotApprove = itemView.findViewById(R.id.btn_not_approve);
            mTvDate = itemView.findViewById(R.id.tvDate);

            mBtnApprove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String ID = (String) mStudentId.getText();
                    final String ISBN = (String) mBookIsbn.getText();
                   final DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("Reservations");
                    myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {

                                for (DataSnapshot d : dataSnapshot.getChildren()) {
                                        String book_isbn = d.child("book_ISBN").getValue(String.class);
                                        String student_id = d.child("student_id").getValue(String.class);

                                        if(book_isbn.equals(ISBN) && student_id.equals(ID)){
                                            bookApprove(d.getKey());
                                        }

                                }
                            } else {
                                Log.d("TAG", "ISBN NO FOUND");
                            }
                        }

                        @Override
                        public void onCancelled(@NotNull DatabaseError databaseError) {

                        }
                    });


//                    String date = new ConnectionCheck(mContext).CurrentDateTime();
//
//                    Toast.makeText(mContext, "Date => "+date, Toast.LENGTH_SHORT).show();


                }
            });


            mBtnNotApprove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String ID = (String) mStudentId.getText();
                    final String ISBN = (String) mBookIsbn.getText();
                    final Query query1 = FirebaseDatabase.getInstance().getReference("Reservations").orderByChild("book_ISBN").equalTo(ISBN);
                    final Query query2 = FirebaseDatabase.getInstance().getReference("Reservations").orderByChild("student_id").equalTo(ID);

                    query1.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                query2.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()) {
                                            String key = "";
                                            for (DataSnapshot d : dataSnapshot.getChildren()) {
                                                key = d.getKey();

                                            }
                                            bookNotApprove(key);
                                        } else {
                                            Log.d("TAG", "ID NO FOUND");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NotNull DatabaseError databaseError) {

                                    }
                                });
                            } else {
                                Log.d("TAG", "ISBN NO FOUND");
                            }
                        }

                        @Override
                        public void onCancelled(@NotNull DatabaseError databaseError) {

                        }
                    });


//                    String date = new ConnectionCheck(mContext).CurrentDateTime();
//
//                    Toast.makeText(mContext, "Date => "+date, Toast.LENGTH_SHORT).show();


                }
            });

        }
        }


        private void bookApprove(String key) {
//        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
//
//            @Override
//            public void onDateSet(final DatePicker view, int year, int monthOfYear,
//                                  int dayOfMonth) {
//                // TODO Auto-generated method stub
//                myCalendar.set(Calendar.YEAR, year);
//                myCalendar.set(Calendar.MONTH, monthOfYear);
//                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//                String myFormat = "dd/MM/yy"; //In which you need put here
//                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
////                Log.d("TAG", "DATE => "+sdf.format(myCalendar.getTime()));
//
//                String currentDate = new ConnectionCheck(mContext).CurrentDate();
//                String currentTime = new ConnectionCheck(mContext).CurrentTime();
//                String returnDate = sdf.format(myCalendar.getTime());
//                Model_Book_Issue data = new Model_Book_Issue(ISBN,ID,currentDate,returnDate,currentTime);
//                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("issued_books");
//                String id = ref.push().getKey();
//                ref.child(id).setValue(data).addOnCompleteListener(new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        Snackbar.make(view,"Book Issued Successfully",Snackbar.LENGTH_SHORT).show();
//
//                    }
//                });
//
//            }
//
//        };
//        new DatePickerDialog(mContext, date, myCalendar
//                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
//        Log.d("TAG", "Ref : "+key);
//        Adding issue book data
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Reservations").child(key).child("status");
//        Log.d("TAG", "Ref : "+ref);
            ref.setValue("approved").addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Toast.makeText(mContext, "Book Approved", Toast.LENGTH_SHORT).show();
                }
            });


        }


    private void bookNotApprove(String key) {
        //        Adding issue book data
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Reservations").child(key).child("status");
        ref.setValue("not approved").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(mContext, "Book Not Approved", Toast.LENGTH_SHORT).show();
            }
        });
    }


        @NonNull
        @Override
        public Adaptor_Reservations.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(mContext).inflate(R.layout.layout_reservations, parent, false);
            return new Adaptor_Reservations.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull final Adaptor_Reservations.ViewHolder holder, int position) {

            Model_Reserve_Book uploadCurrent = mList.get(position);

            holder.mStudentId.setText(uploadCurrent.getStudent_id());
            holder.mBookIsbn.setText(uploadCurrent.getBook_ISBN());
            holder.mTvDate.setText("Date: " + uploadCurrent.getDate());

            DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("Book").child("-H"+uploadCurrent.getBook_ISBN());
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    if(dataSnapshot.exists()){
                            String bookImage = dataSnapshot.child("imageUri").getValue(String.class);
                            String booktitle = dataSnapshot.child("name").getValue(String.class);

                        holder.mTvBookTitle.setText(booktitle);
                        Picasso.get().load(bookImage).into(holder.mBookImage);



                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });



        }

        @Override
        public int getItemCount() {
            return mList.size();
        }


    }




package com.example.codex;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class Adapter_Book_Reviews extends RecyclerView.Adapter<Adapter_Book_Reviews.ViewHolder> {


    List<Model_Book_Review> mList;
    Context mContext;
    ImageView[] starViews;

    public Adapter_Book_Reviews(List<Model_Book_Review> mList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public Adapter_Book_Reviews.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.layout_book_review, parent, false);
        return new Adapter_Book_Reviews.ViewHolder(v);    }

    @Override
    public void onBindViewHolder(@NonNull final Adapter_Book_Reviews.ViewHolder holder, int position) {

        final Model_Book_Review model = mList.get(position);


        DatabaseReference Ref = FirebaseDatabase.getInstance().getReference("registered_users").child("students");
        Query queryName = Ref.orderByChild("university_id").equalTo(model.getUser_id());
        queryName.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){
                    for (DataSnapshot d : dataSnapshot.getChildren()){
                        Model_Student_Detail user = d.getValue(Model_Student_Detail.class);
                        String name = user.getName();
                        holder.mUsername.setText(name);
                        if(!user.getPicture().equals("")){
                            Picasso.get().load( user.getPicture()).into(holder.mImage);
                        }


                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        int numStars = (int) model.getRating();
        holder.mLayout.setTag(numStars);
        holder.mLayout.removeAllViews();
        starViews=new ImageView[5];
        for(int s=0; s<starViews.length; s++){
            starViews[s]=new ImageView(mContext);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(30, 30);
            starViews[s].setLayoutParams(layoutParams);
        }
        for(int s=0; s<numStars; s++){
            starViews[s].setImageResource(R.drawable.star);
            holder.mLayout.addView(starViews[s]);
        }


        holder.mComment.setText(model.getComment());

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView mUsername,mComment;
        CircleImageView mImage;
        LinearLayout mLayout;


        public ViewHolder(@NonNull View itemView) {
          super(itemView);

          mLayout = itemView.findViewById(R.id.star_layout);
          mUsername = itemView.findViewById(R.id.username);
          mComment = itemView.findViewById(R.id.comment);
          mImage = itemView.findViewById(R.id.userImage);


        }
    }
}

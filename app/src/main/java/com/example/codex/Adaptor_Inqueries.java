package com.example.codex;



import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class Adaptor_Inqueries extends RecyclerView.Adapter<Adaptor_Inqueries.ViewHolder> {

    private Context context;
    private List<String> inqueryList;
    private static ClickListener clickListener;

    public Adaptor_Inqueries() {
    }

    public Adaptor_Inqueries(Context context, List<String> inqueryList) {
        this.context = context;
        this.inqueryList = inqueryList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.layout_inquery_list,parent,false);
        return new Adaptor_Inqueries.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String UserName = inqueryList.get(position);
        holder.userName.setText(UserName);
    }

    @Override
    public int getItemCount() {
        return inqueryList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {

        TextView userName;
        public ViewHolder(View itemView) {
            super(itemView);


            userName = itemView.findViewById(R.id.txt_username);


        }


        @Override
        public void onClick(View view) {
            clickListener.onItemClick(getAdapterPosition(),view);
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        Adaptor_Inqueries.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }


}

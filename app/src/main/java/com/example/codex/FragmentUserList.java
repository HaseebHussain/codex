package com.example.codex;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentUserList extends Fragment {

    RecyclerView mUserList;
    ArrayList<Model_Student_Detail> mList;

    public FragmentUserList() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragment_user_list, container, false);
        mUserList = v.findViewById(R.id.UserList);
        mList = new ArrayList<>();

        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("registered_users").child("students");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mList.clear();
                if(dataSnapshot.exists()){
                    for(DataSnapshot d: dataSnapshot.getChildren()){
                        Model_Student_Detail details = d.getValue(Model_Student_Detail.class);
                        mList.add(details);
                    }

                    Adapter_Users_List rev = new Adapter_Users_List(getActivity(),mList);
                    mUserList.setAdapter(rev);


                    LinearLayoutManager layoutManager = new
                            LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                    mUserList.setLayoutManager(layoutManager);
                }else{
                    Toast.makeText(getActivity(), "No user found !", Toast.LENGTH_LONG).show();
                }



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        return v;
    }

}

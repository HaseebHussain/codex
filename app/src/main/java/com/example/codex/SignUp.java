package com.example.codex;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.LocusId;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Text;

import java.io.DataInput;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

public class SignUp extends AppCompatActivity {

    TextView med_username,med_password,med_studentid;

    Button mbtn_signup;
    ProgressDialog mprocess;
    List<String> studentIds;
    Spinner mDepartmentSpinner,mGenderSpinner;
    List<String> departments,genders;
    EditText mFullName,mFatherName,mPhone,mEmail,mBatch,mBatchYear,mCnic,mPassword,mCPassword,mAddress;
    DatabaseReference myRef;
    EditText[] inputFields;
    RelativeLayout mMainLayout;
    private Calendar calendar;
    private SimpleDateFormat dateFormat;
    private String date,UserIdKey;
    ImageView imageView;
    private String university_id,id;
    ConnectionCheck check;
    DatabaseReference UserIDRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        med_studentid = findViewById(R.id.ed_studentid);
        inputFields = new EditText[10];
        university_id = getIntent().getExtras().getString("user_id");
        UserIdKey = getIntent().getExtras().getString("userIdKey");

        if(UserIdKey != null){
            UserIDRef = FirebaseDatabase.getInstance().getReference("users").child(UserIdKey);
        }

        //Call view Element Method
        CallViewItems();
        check = new ConnectionCheck(this);

        departments.add("Business Administration");
        departments.add("Commerce");
        departments.add("English");
        departments.add("Education");
        departments.add("Information Technology");
        departments.add("Computer Science");
        departments.add("Pharmacy");
        ArrayAdapter<String> departmentAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,departments);
        mDepartmentSpinner.setAdapter(departmentAdapter);

        genders.add("Male");
        genders.add("Female");
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,genders);
        mGenderSpinner.setAdapter(genderAdapter);

        mbtn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                CheckStudentID();
                UserDetails();

            }
        });

//        set validation to phone
        mPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mPhone.getText().toString().length() > 11) {
                    mPhone.setError("Phone number must be of 11 numbers");
                } else {
                    mPhone.setError(null);
                }
            }
        });

//        check cnic
        mCnic.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mCnic.getText().toString().length() > 13) {
                    mCnic.setError("CNIC number must be of 11 numbers");
                } else {
                    mCnic.setError(null);
                }
            }
        });

        mPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mCnic.getText().toString().length() < 6) {
                    mCnic.setError("Password must be of 11 numbers");
                } else {
                    mCnic.setError(null);
                }
            }
        });

        mEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!isEmailValid(s)){
                    mEmail.setError("Invalid Email Address..");
                }
            }
        });
    }

    boolean isEmailValid(CharSequence s) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(s).matches();
    }
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        //updateUI(currentUser);
    }

    public void CallViewItems(){
        myRef = FirebaseDatabase.getInstance().getReference("registered_users");
        mMainLayout = findViewById(R.id.mainLayout);
        med_username = findViewById(R.id.ed_username);
        med_password = findViewById(R.id.ed_password);
        med_studentid = findViewById(R.id.ed_studentid);
        mbtn_signup = findViewById(R.id.btn_signup);
        mprocess = new ProgressDialog(this);
        //initializing List that contains Student Id to check
        studentIds = new ArrayList<>();
        mDepartmentSpinner = findViewById(R.id.departments);
        departments = new ArrayList<>();
        mGenderSpinner = findViewById(R.id.genders);
        genders = new ArrayList<>();
        mFullName = findViewById(R.id.fullname);
        mFatherName = findViewById(R.id.fathername);
        mPhone = findViewById(R.id.phone);
        mEmail = findViewById(R.id.email);
        mBatch = findViewById(R.id.batch);
        mBatchYear = findViewById(R.id.batch_year);
        mCnic = findViewById(R.id.cnic);
        mPassword = findViewById(R.id.password);
        mCPassword = findViewById(R.id.confirmPassword);
        mAddress = findViewById(R.id.address);
        calendar = Calendar.getInstance();

        inputFields[0] = mFullName;
        inputFields[1] = mFatherName;
        inputFields[2] = mPhone;
        inputFields[3] = mEmail;
        inputFields[4] = mBatch;
        inputFields[5] = mBatchYear;
        inputFields[6] = mCnic;
        inputFields[7] = mPassword;
        inputFields[8] = mCPassword;
        inputFields[9] = mAddress;



    }


    private boolean validate(EditText[] fields){
        for(int i = 0; i < fields.length; i++){
            EditText currentField = fields[i];
            if(currentField.getText().toString().length() <= 0){
                String tag = currentField.getTag().toString();
                Toast.makeText(this, tag+" field is empty", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return true;
    }

    public void CheckStudentID(){
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Student_Details");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NotNull DataSnapshot dataSnapshot) {

                for (DataSnapshot ds: dataSnapshot.getChildren() ){
                    String student_details = ds.child("student_id").getValue(String.class);
                    studentIds.add(student_details);
                }

                //Check User Validation of User
                RegisterUser(studentIds);
            }

            @Override
            public void onCancelled(@NotNull DatabaseError databaseError) {
                Toast.makeText(SignUp.this, "No Data Found", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void RegisterUser(List<String> studentIds){
        String studentid = med_studentid.getText().toString();
        int size =  studentIds.size();
        Log.d("size", "RegisterUser: "+ size);
        if(TextUtils.isEmpty(studentid)){
            Toast.makeText(this, "Please insert student id", Toast.LENGTH_SHORT).show();
        }else {
            if(studentIds.contains(studentid)){
                UserDetails();
            }else{
                Toast.makeText(this, "Sorry ID does'nt exist", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void GoToLogin(View view){
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }

    public void UserDetails(){
        mprocess.setTitle("Please wait...");
        mprocess.show();
        final DatabaseReference myRefStd = myRef.child("students");
        id = myRefStd.push().getKey();

        String sfullname = mFullName.getText().toString();
        String sfathername = mFatherName.getText().toString();
        String sphone = mPhone.getText().toString();
        String semail = mEmail.getText().toString();
        String sDepartment = mDepartmentSpinner.getSelectedItem().toString();
        String sBatch = mBatch.getText().toString();
        String sBatchyear = mBatchYear.getText().toString();

        String sCnic = mCnic.getText().toString();
        String sGender = mGenderSpinner.getSelectedItem().toString();
        String sPassword = mPassword.getText().toString();
        String sCPassword = mCPassword.getText().toString();
        String sAddress = mAddress.getText().toString();
        dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        date = dateFormat.format(calendar.getTime());


        if(check.RequestInternetConnection()){

            if(validate(inputFields)){

                if(sPassword.equals(sCPassword)){
                    String device_instance = FirebaseInstanceId.getInstance().getToken();
                    final Model_Student_Detail detail = new Model_Student_Detail(id,university_id,date,sfullname,sfathername,sAddress,sphone,semail,sPassword,sDepartment,sBatch,sBatchyear,sCnic,sGender,"",device_instance,0,"offline",false);
                    FirebaseAuth auth = FirebaseAuth.getInstance();
                    auth.createUserWithEmailAndPassword(semail,sPassword)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    myRefStd.child(id).setValue(detail)
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    mprocess.dismiss();
                                                    Snackbar.make(mMainLayout,"Registered Successfully",Snackbar.LENGTH_SHORT).show();
                                                    Dialog dialog = new Dialog(SignUp.this);
                                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                                    dialog.setContentView(R.layout.layout_add_profile_image);

                                                    MaterialButton mSetImage = dialog.findViewById(R.id.setImage);
                                                    MaterialButton mSkip = dialog.findViewById(R.id.skip);
                                                    imageView = dialog.findViewById(R.id.image);
                                                    mSetImage.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                          if(check.RequestInternetConnection()){
                                                              setProfileImage();
                                                          }else{
                                                              Toast.makeText(SignUp.this, "Internet is not connected", Toast.LENGTH_LONG).show();
                                                          }
                                                        }
                                                    });
                                                    imageView.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            OpenGallery();
                                                        }
                                                    });
                                                    mSkip.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            setDefaultImage();

                                                        }
                                                    });

                                                    dialog.show();
                                                }
                                            });
                                }
                            });

                }else{
                    Snackbar.make(mMainLayout,"Password is not matched !",Snackbar.LENGTH_SHORT).show();
                    mprocess.dismiss();
                }

            }else{
                mprocess.dismiss();
            }

        }else{
            Snackbar.make(mMainLayout,"No Internet ! ",Snackbar.LENGTH_LONG).show();
        }
    }

    private void setProfileImage() {
        mprocess.setMessage("Please Wait...");
        mprocess.show();
        final String imageUri = PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                .getString("profileImageUri",null);
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().remove("profileImageUri").apply();
        myRef.child("students").child(id).child("picture").setValue(imageUri)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        Snackbar.make(mMainLayout,"Profile Image set!",Snackbar.LENGTH_SHORT).show();
                        Snackbar.make(mMainLayout,"Login to start using codex...!",Snackbar.LENGTH_SHORT).show();

                        UserIDRef.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                                Intent intent = new Intent(SignUp.this, Login.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                mprocess.dismiss();
                                startActivity(intent);
                            }
                        });



                    }
                });


    }


    private void OpenGallery(){
//        ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.ACCESS_NETWORK_STATE},0);
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(Intent.createChooser(galleryIntent,"Select Image from Gallery"),1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
                {
                    Uri uri = data.getData();
                    imageView.setImageURI(uri);
//                    imgProfileImage.setImageURI(uri);
                    StorageReference mStorage = FirebaseStorage.getInstance().getReference("profile_images");


                    if (uri != null) {
                        mStorage.child(mEmail.getText().toString()).putFile(uri);
                        mStorage.child(mEmail.getText().toString()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                final String Imageuri = uri.toString();
                                PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                                .edit().putString("profileImageUri",Imageuri).apply();

                            }
                        });
                    }


                }

            }else
            {
                Toast.makeText(SignUp.this, "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        } catch(Exception e){
            Toast.makeText(SignUp.this, e.toString(), Toast.LENGTH_LONG)
                    .show();

        }
    }


    private void setDefaultImage(){
       try{
           mprocess.setMessage("Please Wait...");
           mprocess.show();

                   String imageUri = "";
                   if(mGenderSpinner.getSelectedItem() == "Male"){
                       imageUri = "https://firebasestorage.googleapis.com/v0/b/codex-8dada.appspot.com/o/profile_images%2Fdefault.png?alt=media&token=62aea3b6-0159-472d-b58b-b09257657edc";
                   }else{
                       imageUri = "https://firebasestorage.googleapis.com/v0/b/codex-8dada.appspot.com/o/profile_images%2Fdefault-girl.png?alt=media&token=ebf14bba-6c2c-42c9-a9f8-e1a302c63560";
                   }
                   myRef.child("students").child(id).child("picture").setValue(imageUri)
                           .addOnCompleteListener(new OnCompleteListener<Void>() {
                               @Override
                               public void onComplete(@NonNull Task<Void> task) {
                                   UserIDRef.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                       @Override
                                       public void onComplete(@NonNull Task<Void> task) {
                                           Snackbar.make(mMainLayout,"Login to start using codex...!",Snackbar.LENGTH_SHORT).show();
                                           Intent intent = new Intent(SignUp.this, Login.class);
                                           intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                           mprocess.dismiss();
                                           startActivity(intent);
                                       }
                                   });
                               }
                           });

        }catch (Exception e){
           e.printStackTrace();
       }
    }



}


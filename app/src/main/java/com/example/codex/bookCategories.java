package com.example.codex;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class bookCategories extends AppCompatActivity {



    ImageView mImgDepartment;
    TextView mTvDepartment;
    ArrayList<String> Category_List;
    ListView mListCategories;
    ArrayAdapter<String> categoryAdaptor;
    Query query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_categories);

        Category_List = new ArrayList<>();
        mListCategories = findViewById(R.id.listCategories);


        String department = getIntent().getStringExtra("department");
        int Image_Res = getIntent().getExtras().getInt("image");

        mImgDepartment = findViewById(R.id.imgDepartment);
        mImgDepartment.setImageResource(Image_Res);

        mTvDepartment = findViewById(R.id.tvDepartment);
        mTvDepartment.setText(department);

//        Adding data to array list
//        Category_List.add("Computer Science");
//        Category_List.add("Business Admistration");
//        Category_List.add("English");



//        Getting categories list
        query = FirebaseDatabase.getInstance().getReference("Book").orderByChild("subject").equalTo(department);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<String> temp = new ArrayList<>();
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    String category = d.child("categories").getValue(String.class);
                    if (!temp.contains(category)) {
                        temp.add(category);
                    }
                    Log.d("TAG", "onDataChange: " + temp);


                }
                if(temp.isEmpty()){
                    temp.add("No Categories Found !");
                    categoryAdaptor = new ArrayAdapter<>(bookCategories.this,
                            android.R.layout.simple_list_item_activated_1, temp);

                    mListCategories.setAdapter(categoryAdaptor);
                }else{
                    categoryAdaptor = new ArrayAdapter<>(bookCategories.this,
                            android.R.layout.simple_list_item_activated_1, temp);
                    mListCategories.setDividerHeight(2);
                    mListCategories.setAdapter(categoryAdaptor);

                    mListCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String title = mListCategories.getItemAtPosition(position).toString();
                           Intent intent = new Intent(bookCategories.this,Books_List.class);
                           intent.putExtra("category",title);
                           startActivity(intent);
                        }
                    });
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });





    }
}

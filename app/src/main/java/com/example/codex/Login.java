package com.example.codex;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;


import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import de.hdodenhof.circleimageview.CircleImageView;
import pl.droidsonroids.gif.GifImageView;

import static android.widget.Toast.LENGTH_SHORT;

@SuppressWarnings("ALL")
public class Login extends AppCompatActivity {

    static List<String> usernames,passwords,std_ids,std_fullnames;
    EditText med_username,med_password;
    GifImageView progressBar;
    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;
    String username,password;
    static boolean check = false;
    DatabaseReference myref;
    TextView imageView;
    String name,user_id;
    LinearLayout layout;
    SharedPreferences.Editor editor;
    SharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editor = getSharedPreferences("codex",MODE_PRIVATE).edit();
        preferences = getSharedPreferences("codex",MODE_PRIVATE);
        CallViewItems();

        //Admin Already Login
        final FirebaseUser user = firebaseAuth.getCurrentUser();
        name = preferences.getString("student_id","NULL");
        String category = preferences.getString("category","NULL");

        user_id = PreferenceManager.getDefaultSharedPreferences(this).getString("User_ID","Not Found");
//        if(user != null){
//            Intent intent = new Intent(Login.this, Admin_Panel.class);
//            startActivity(intent);
//        }else if(!name.equals("") && !user_id.equals("")){
//            Intent intent = new Intent(Login.this, UserPanel.class);
//            intent.putExtra("user","student");
//            startActivity(intent);
//        }
        if(!name.equals("NULL")){
            if(category.equals("student")){
            Intent intent = new Intent(Login.this, UserPanel.class);
            intent.putExtra("user","student");
            startActivity(intent);
            }else if(category.equals("admin")){
                            Intent intent = new Intent(Login.this, Admin_Panel.class);
            startActivity(intent);
            }
        }

//        imageView.setImageResource(R.drawable.codex);
        Animation slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);
        imageView.startAnimation(slide_up);
        final LinearLayout linearLayout = findViewById(R.id.layout);
        linearLayout.setVisibility(View.VISIBLE);
        Animation fade_in = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_in);
        linearLayout.startAnimation(fade_in);


//        Custom Font

//        Typeface loster = Typeface.createFromAsset(getAssets(),"font/lobster.ttf");


        Button button = findViewById(R.id.btn_Login);
//        button.setTypeface(loster);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ConnectionCheck()){

                    progressBar.setVisibility(View.VISIBLE);
                    layout.setVisibility(View.GONE);
                    imageView.setVisibility(View.GONE);
                    username= med_username.getText().toString().toLowerCase();
                    password = med_password.getText().toString().toLowerCase();
                    myref = FirebaseDatabase.getInstance().getReference("registered_users").child("students");
                    Query queryUser = myref.orderByChild("email").equalTo(username);

                    if(username.equals("admin@gmail.com") && password.equals("admin123")){

                        progressDialog.setMessage("Login in...");
                        progressDialog.show();
                        final String username = med_username.getText().toString().toLowerCase().trim();
                        final String password = med_password.getText().toString().toLowerCase().trim();
                        firebaseAuth.signInWithEmailAndPassword(username, password)
                                .addOnCompleteListener(Login.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            String instance = FirebaseInstanceId.getInstance().getToken();
                                            DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("admin");
                                            myRef.child("device_instance").setValue(instance);
                                            editor.putString("Username", "admin").apply();
                                            editor.putString("PASSWORD",password).apply();
                                            editor.putString("category","admin").apply();
                                            String c = preferences.getString("category","null");
                                            Log.i("TAG", "onComplete: "+c);
                                            Intent intent = new Intent(Login.this,Admin_Panel.class);
                                            startActivity(intent);
                                            progressBar.setVisibility(View.GONE);
                                            progressDialog.dismiss();
                                        } else {
                                            Toast.makeText(Login.this, "Invalid User", LENGTH_SHORT).show();
                                            progressBar.setVisibility(View.GONE);
                                        }
                                    }
                                });
                    }
                    else{
                        queryUser.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                progressBar.setVisibility(View.VISIBLE);
                                if(dataSnapshot.exists()){
                                    for(DataSnapshot ds: dataSnapshot.getChildren()){
                                        Model_Student_Detail modelRegisterUser = ds.getValue(Model_Student_Detail.class);
                                        String sfullname = modelRegisterUser.getName();
                                        String student_id = modelRegisterUser.getStudent_id();
                                        editor.putString("student_id",modelRegisterUser.getStudent_id());
                                        editor.putString("fullname",modelRegisterUser.getName());
                                        editor.putString("email",modelRegisterUser.getEmail());
                                        editor.putString("device_instance",modelRegisterUser.getDevice_instance());
                                        editor.putString("picture",modelRegisterUser.getPicture());
                                        editor.putString("university_id",modelRegisterUser.getUniversity_id());
                                        editor.putString("category","student");
                                        editor.apply();
                                        new Custom_Methods().updateDeviceInstance(student_id);
                                            openHomeActivity(username,password);

                                        }

//                                    checkUserInfo(usernames,passwords,std_ids,username,password,std_fullnames);
                                }else{
                                    Toast.makeText(Login.this, "Invalid User", LENGTH_SHORT).show();
                                    linearLayout.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                }

                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                Toast.makeText(Login.this, "User Not Found", Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                                layout.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }else{
                    RequestInternetConnection();
                    progressBar.setVisibility(View.GONE);
                    layout.setVisibility(View.VISIBLE);
                    imageView.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    private boolean ConnectionCheck(){
        ConnectivityManager cm =
                (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);

        assert cm != null;
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return (activeNetwork != null &&  activeNetwork.isConnectedOrConnecting());
    }

    private void RequestInternetConnection(){
        ConnectivityManager check = (ConnectivityManager)
                this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] info = check.getAllNetworkInfo();

        for (int i = 0; i<info.length; i++){
            if (info[i].getState() == NetworkInfo.State.CONNECTED){
                Toast.makeText(this, "Internet is connected",Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(this, "Internet is not connected",Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
                break;
            }
        }
    }

    private void checkUserInfo(List<String> usernames,List<String> passwords,List<String> student_ids,
                               String username,String password,List<String> fullNames){




        for(int i = 0 ; i <usernames.size(); i++) {
            if (usernames.get(i).equals(username) && passwords.get(i).equals(password)) {

                PreferenceManager.getDefaultSharedPreferences(this).edit().putString("User_ID", student_ids.get(i)).apply();
                PreferenceManager.getDefaultSharedPreferences(this).edit().putString("Username", fullNames.get(i)).apply();
                PreferenceManager.getDefaultSharedPreferences(this).edit().putString("category","user").apply();


//                openHomeActivity(student_ids.get(i),usernames.get(i),passwords.get(i));

                check = true;
                break;
            }


        }
        if(!check){
            Toast.makeText(this, "User not found", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);
            layout.setVisibility(View.VISIBLE);
        }
    }

    private void openAdminActivity(String username,String password) {







    }




    public void CallViewItems(){
        usernames = new ArrayList<>();
        passwords = new ArrayList<>();
        std_ids = new ArrayList<>();
        std_fullnames = new ArrayList<>();
        med_username = findViewById(R.id.ed_username);
        med_password = findViewById(R.id.ed_password);
        progressBar = findViewById(R.id.progress);
        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
        imageView = findViewById(R.id.img_logo);
        layout = findViewById(R.id.layout);


    }

    public void openHomeActivity(String email, String password)
    {

        FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressBar.setVisibility(View.GONE);

                Intent intent = new Intent(Login.this,UserPanel.class);
                intent.putExtra("user","student");
                startActivity(intent);

            }
        });
    }



    public void GoToSignup(View view)
    {
        Dialog d = new Dialog(this);
        d.setContentView(R.layout.layout_choose_member);
        CircleImageView mEmployee = d.findViewById(R.id.employee);
        CircleImageView mStudent = d.findViewById(R.id.student);

//        if employee button is click
        mEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (getApplicationContext(), Employee_Signup.class);
                startActivity(intent);
            }
        });

//        if student button is click
        mStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        Intent intent = new Intent (getApplicationContext(), checkUserID.class);
                        startActivity(intent);
            }
        });

        d.show();
//        new AlertDialog.Builder(this)
//                .setTitle("Signup")
//                .setMessage("Please select the type of user...")
//                .setPositiveButton("Student", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        Intent intent = new Intent (getApplicationContext(), checkUserID.class);
//                        startActivity(intent);
//                    }
//                })
//                .setNegativeButton("Employee", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        Intent intent = new Intent (getApplicationContext(), Employee_Signup.class);
//                        startActivity(intent);
//                    }
//                })
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .show();
    }

    public boolean CheckUser(List<String> usernames,List<String> passwords){
        String username = med_username.getText().toString();
        String password = med_password.getText().toString();
        for(int i = 0; i<=usernames.size();i++) {
            if (username.equals(usernames.get(i))) {
                return true;
            }
        }
        return false;
    }




}


package com.example.codex;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import okhttp3.internal.Util;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.contentcapture.DataRemovalRequest;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class checkUserID extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference reference;
    ProgressBar progressBar;
    Button mCheckID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_user_id);

        database = FirebaseDatabase.getInstance();
        reference = database.getReference("users");
        progressBar = findViewById(R.id.waiting);

        mCheckID = findViewById(R.id.check_id);
        mCheckID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                try{
                    progressBar.setVisibility(View.VISIBLE);
                    v.setVisibility(View.GONE);
                    TextInputEditText mUserId = findViewById(R.id.userId);
                    final String student_id = mUserId.getText().toString().trim();
                    if(TextUtils.isEmpty(student_id)){
                        Toast.makeText(checkUserID.this, "Please enter the ID first", Toast.LENGTH_SHORT).show();
                    }else {
                        Query query = reference.orderByChild("reg_no").equalTo(student_id);
                        query.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    for (DataSnapshot d : dataSnapshot.getChildren()) {
                                        String id = d.child("reg_no").getValue(String.class);
                                        if (id != null) {
                                            if (id.equals(student_id)) {
                                                Intent intent = new Intent(checkUserID.this,SignUp.class);
                                                intent.putExtra("user_id",student_id);
                                                intent.putExtra("userIdKey",d.getKey());
                                                startActivity(intent);
//                                                DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("users");
//                                                userRef.child(key).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
//                                                    @Override
//                                                    public void onComplete(@NonNull Task<Void> task) {
//
//                                                    }
//                                                });
                                            }
                                        }
                                    }
//                                if(student_id.equals(id)){
//                                    Intent intent = new Intent(checkUserID.this,SignUp.class);
//                                    intent.putExtra("user_id",id);
//                                    startActivity(intent);
//                                }
                                } else {
                                    Toast.makeText(checkUserID.this, "ID not found", Toast.LENGTH_SHORT).show();
                                    v.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

    }


}

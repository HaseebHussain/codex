package com.example.codex;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Previously_Borrowed_Books extends AppCompatActivity {

    SharedPreferences preferences;
    ListView mList;
    TextView mMessage;
    List<Model_Book_Return> mHistoryList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previously__borrowed__books);


        preferences = getSharedPreferences("codex",MODE_PRIVATE);
        mList = findViewById(R.id.list);
        mMessage = findViewById(R.id.message);
        mHistoryList = new ArrayList<>();

        String student_id = preferences.getString("student_id","null");
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setTitle("Previous Book Issued");

        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("books_transactions");
        Query query = myRef.orderByChild("user_id").equalTo(student_id);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mHistoryList.clear();
                if(dataSnapshot.exists()){
                    for(DataSnapshot d: dataSnapshot.getChildren()){
                        Model_Book_Return book_return = d.getValue(Model_Book_Return.class);
                        mHistoryList.add(book_return);
                    }
                    Adapter_Users_Previous_Books adapter = new Adapter_Users_Previous_Books(Previously_Borrowed_Books.this,mHistoryList);
                    mList.setAdapter(adapter);
                }else{
                    mMessage.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.student_home_menu,menu);
        MenuItem search= menu.findItem(R.id.app_bar_search);
        SearchView searchView = (SearchView) search.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Toast.makeText(Previously_Borrowed_Books.this, newText, Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        return true;
    }

}

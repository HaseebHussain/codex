package com.example.codex;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.nex3z.notificationbadge.NotificationBadge;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

import androidx.annotation.NonNull;

public class Adapter_Users_Inquery_List extends BaseAdapter {

    List<String> mList;
    Context mContext;

    public Adapter_Users_Inquery_List(List<String> mList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View gridView;

        final String book_details = mList.get(position);

        if (convertView == null) {

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.layout_inquery_list, null);

            final TextView mUsername = gridView.findViewById(R.id.txt_username);
            final TextView mStudendId = gridView.findViewById(R.id.StudentId);
            mStudendId.setText(book_details);

//            Getting student name
            Query queryName = FirebaseDatabase.getInstance().getReference("registered_users").child("students");
            queryName.orderByChild("university_id").equalTo(book_details)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                           for(DataSnapshot d : dataSnapshot.getChildren()){
                               Model_Student_Detail modelStudentDetails = d.getValue(Model_Student_Detail.class);
                               mUsername.setText(modelStudentDetails.getName());
                           }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

            //            Getting student name
            Query queryCount = FirebaseDatabase.getInstance().getReference("Inquiries");
            queryCount.orderByChild("chat_id").equalTo(book_details)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            int number=0;
                         if(dataSnapshot.exists()){
                             for(DataSnapshot d : dataSnapshot.getChildren()){
                                 Model_Inquery inquery = d.getValue(Model_Inquery.class);
                                 if(!inquery.isRead()){
                                     number++;
                                 }
                             }
                             NotificationBadge mUreadMessages = gridView.findViewById(R.id.UnreadMessageCount);
                             mUreadMessages.setNumber(number);
                         }else{
                             NotificationBadge mUreadMessages = gridView.findViewById(R.id.UnreadMessageCount);
                             mUreadMessages.setNumber(0);
                         }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });










        } else {
            gridView = convertView;
        }

        gridView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,AdminInquieryDetails.class);
                intent.putExtra("name",book_details);
                mContext.startActivity(intent);
            }
        });

        return gridView;
    }
}

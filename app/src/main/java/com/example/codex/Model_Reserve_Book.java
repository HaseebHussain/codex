package com.example.codex;


public class Model_Reserve_Book {
    String reservation_id;
    String student_id;
    String student_name;
    String book_ISBN;
    String book_title;
    String book_image;
    String Date;
    String status;
    String device_instance;

    public Model_Reserve_Book() {
    }

    public Model_Reserve_Book(String reservation_id, String student_id, String student_name, String book_ISBN, String book_title, String book_image, String date, String status, String device_instance) {
        this.reservation_id = reservation_id;
        this.student_id = student_id;
        this.student_name = student_name;
        this.book_ISBN = book_ISBN;
        this.book_title = book_title;
        this.book_image = book_image;
        Date = date;
        this.status = status;
        this.device_instance = device_instance;
    }

    public String getReservation_id() {
        return reservation_id;
    }

    public void setReservation_id(String reservation_id) {
        this.reservation_id = reservation_id;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getBook_ISBN() {
        return book_ISBN;
    }

    public void setBook_ISBN(String book_ISBN) {
        this.book_ISBN = book_ISBN;
    }

    public String getBook_title() {
        return book_title;
    }

    public void setBook_title(String book_title) {
        this.book_title = book_title;
    }

    public String getBook_image() {
        return book_image;
    }

    public void setBook_image(String book_image) {
        this.book_image = book_image;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDevice_instance() {
        return device_instance;
    }

    public void setDevice_instance(String device_instance) {
        this.device_instance = device_instance;
    }
}

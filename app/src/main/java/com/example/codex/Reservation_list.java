package com.example.codex;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class Reservation_list extends AppCompatActivity {

    RecyclerView recyclerView;
    private List<Model_Reserve_Book> mReserve_Books;
    private ValueEventListener mDBListener;
    private FirebaseStorage mStorage;
    private DatabaseReference mDatabaseRef;
    private Adaptor_Reservations mAdapter;
    private ProgressBar progressBar;
    LinearLayout constraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation_list);
        recyclerView = findViewById(R.id.rv_reservation);

        mReserve_Books  = new ArrayList<>();
        mDatabaseRef = FirebaseDatabase.getInstance().getReference();
        progressBar = findViewById(R.id.process);
        constraintLayout = findViewById(R.id.constraintLayout);

        ConnectionCheck cc = new ConnectionCheck(this);

        boolean connection = cc.RequestInternetConnection();

        progressBar.setVisibility(View.VISIBLE);

        if(connection){

            mDatabaseRef.child("Reservations").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    mReserve_Books.clear();

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                        Model_Reserve_Book upload = postSnapshot.getValue(Model_Reserve_Book.class);
                        String status = postSnapshot.child("status").getValue(String.class);

                        if(status.equals("pending")){
                            mReserve_Books.add(upload);
                        }
                    }


                    if(mReserve_Books.isEmpty()){
                        Snackbar.make(constraintLayout,"No reservations pending", Snackbar.LENGTH_LONG)
                                .show();
                    }else{
                        Adaptor_Reservations rev = new Adaptor_Reservations(Reservation_list.this,mReserve_Books);
                        recyclerView.setAdapter(rev);


                        LinearLayoutManager layoutManager = new
                                LinearLayoutManager(Reservation_list.this, LinearLayoutManager.VERTICAL, false);
                        recyclerView.setLayoutManager(layoutManager);
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(Reservation_list.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        }else{
            Toast.makeText(this, "Internet is not connected", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);
        }



    }
}

package com.example.codex;

import android.app.DatePickerDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class Adaptor_Book_Approved extends RecyclerView.Adapter<Adaptor_Book_Approved.ViewHolder> {

    private Context mContext;
    private List<Model_Reserve_Book> mList;

    public Adaptor_Book_Approved(Context mContext, List<Model_Reserve_Book> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public Adaptor_Book_Approved.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.layout_book_approved, parent, false);
        return new Adaptor_Book_Approved.ViewHolder(v);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView mTvStudentName,mTvBookTitle,mRequestStatus;
        ImageView mImgBookImage;
        Button mBookIssue;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mBookIssue = itemView.findViewById(R.id.btnIssueBook);
            mTvStudentName = itemView.findViewById(R.id.tv_student_name);
            mTvBookTitle = itemView.findViewById(R.id.tv_book_title);
            mRequestStatus = itemView.findViewById(R.id.tv_status);
            mImgBookImage = itemView.findViewById(R.id.img_book_image);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull Adaptor_Book_Approved.ViewHolder holder, int position) {

        final Model_Reserve_Book uploadCurrent = mList.get(position);
        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

        holder.mTvStudentName.setText(uploadCurrent.getStudent_name());
        holder.mTvBookTitle.setText(uploadCurrent.getBook_title());

        holder.mRequestStatus.setText(uploadCurrent.getStatus());
        if(!uploadCurrent.getBook_image().isEmpty()){
            Picasso.get().load(uploadCurrent.getBook_image()).into(holder.mImgBookImage);
        }

        holder.mTvBookTitle.setSelected(true);
        holder.mTvStudentName.setSelected(true);

        holder.mBookIssue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                IssueBook(uploadCurrent.getBook_ISBN(),uploadCurrent.getStudent_id(),uploadCurrent.getReservation_id(),reference);

            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public void IssueBook(String book_isbn, final String university_id, final String ID, final DatabaseReference reference){

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH)+6);
        Date date = cal.getTime();
        String returnDate = df.format(date);

        String currentDate = new ConnectionCheck(mContext).CurrentDate();
        String currentTime = new ConnectionCheck(mContext).CurrentTime();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("issued_books");
        Log.i("TAG", " Return Date : "+returnDate);
        String id = ref.push().getKey();
        Model_Book_Issue data = new Model_Book_Issue(id,book_isbn,university_id,currentDate,returnDate,currentTime,"issued");
        ref.child(id).setValue(data).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                reference.child("Reservations").child(ID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        Toast.makeText(mContext, "Book Issued Successfully", Toast.LENGTH_SHORT).show();

                    }
                });

            }
        });
    }

}

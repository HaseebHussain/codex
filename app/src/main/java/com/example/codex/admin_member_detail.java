package com.example.codex;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Objects;

public class admin_member_detail extends AppCompatActivity {

    MaterialToolbar mToolbar;
    FloatingActionButton mEdit_Profile;
    SharedPreferences preferences;
    String email;
    ImageView mUpdateImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_member_detail);

        mEdit_Profile = findViewById(R.id.edit_profile);
        mUpdateImage = findViewById(R.id.update_image);
        preferences = getSharedPreferences("codex",MODE_PRIVATE);
        email = preferences.getString("email","null");
        String category = preferences.getString("category","null");
        if(category.equals("admin")){
            mEdit_Profile.setVisibility(View.VISIBLE);
            mUpdateImage.setVisibility(View.GONE);
        }

        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final String student_id = getIntent().getExtras().getString("student_id");

        final ProgressBar mWaiting =  findViewById(R.id.waiting);
        mWaiting.setVisibility(View.VISIBLE);
        final CircleImageView mPicture = findViewById(R.id.picture);
        final TextView mMemberName = findViewById(R.id.member_name);
        final TextView mUniversityId = findViewById(R.id.university_id);
        final TextView mFatherName = findViewById(R.id.father_name);
        final TextView mGender = findViewById(R.id.gender);
        final TextView mEmail = findViewById(R.id.email);
        final TextView mPassword = findViewById(R.id.password);
        final TextView mDepartment = findViewById(R.id.department);
        final TextView mCnic = findViewById(R.id.cnic);
        final TextView mPhone = findViewById(R.id.phone);
        final TextView mRegDate = findViewById(R.id.reg_date);
        final TextView mAddress = findViewById(R.id.address);
        final TextView mBatch = findViewById(R.id.batch);
        final TextView mBatchYear = findViewById(R.id.batch_year);
        final TextView mBookIssue = findViewById(R.id.book_issued);
        final TextView mStatus = findViewById(R.id.status);
        final TextView mBlocked = findViewById(R.id.blocked);



        DatabaseReference StdRef = FirebaseDatabase.getInstance().getReference("registered_users").child("students");
        StdRef.child(student_id)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        try{
                            if(dataSnapshot.exists()){


                                String picture = dataSnapshot.child("picture").getValue(String.class);
                                assert picture != null;
                                if(!picture.equals("")){
                                    Picasso.get().load(picture).into(mPicture);
                                }
                                String university_id = dataSnapshot.child("university_id").getValue(String.class);
                                String name = dataSnapshot.child("name").getValue(String.class);
                                String fathername = dataSnapshot.child("father_name").getValue(String.class);
                                String gender = dataSnapshot.child("gender").getValue(String.class);
                                String email = dataSnapshot.child("email").getValue(String.class);
                                String password = dataSnapshot.child("password").getValue(String.class);
                                String department = dataSnapshot.child("department").getValue(String.class);
                                String cnic = dataSnapshot.child("cnic").getValue(String.class);
                                String phone_no = dataSnapshot.child("phone_no").getValue(String.class);
                                String registration_date = dataSnapshot.child("registratin_date").getValue(String.class);
                                String address = dataSnapshot.child("address").getValue(String.class);
                                String batch = dataSnapshot.child("batch").getValue(String.class);
                                String batch_year = dataSnapshot.child("batch_year").getValue(String.class);
                                Integer book_issued = dataSnapshot.child("book_issued").getValue(Integer.class);
                                String status = dataSnapshot.child("status").getValue(String.class);
                                boolean block = dataSnapshot.child("blocked").getValue(Boolean.class);

                                mUniversityId.setText(university_id);
                                mMemberName.setText(name);
                                mFatherName.setText(fathername);
                                mEmail.setText(email);
                                mPassword.setText(password);
                                mDepartment.setText(department);
                                mGender.setText(gender);
                                mCnic.setText(cnic);
                                mPhone.setText(phone_no);
                                mRegDate.setText(registration_date);
                                mAddress.setText(address);
                                mBatch.setText(batch);
                                mBatchYear.setText(batch_year);
                                mBookIssue.setText(String.valueOf(book_issued));
                                mStatus.setText(status);

                                if(block){
                                    mBlocked.setText("Yes");
                                }else{
                                    mBlocked.setText("No");
                                }


                                mWaiting.setVisibility(View.GONE);
                            }else{
                                mWaiting.setVisibility(View.GONE);
                                Toast.makeText(admin_member_detail.this, "No Details Found", Toast.LENGTH_SHORT).show();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

        mEdit_Profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(admin_member_detail.this,Edit_Member.class);
                intent.putExtra("student_id",student_id);
                startActivity(intent);
            }
        });
    }

    public void UpdateImage(View view) {

        Crop.pickImage(this);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Uri uri = null;
            StorageReference mStorage = FirebaseStorage.getInstance().getReference("profile_images");
            // When an Image is picked
            if (requestCode == Crop.REQUEST_PICK && resultCode == Activity.RESULT_OK) {
                {
                    uri = data.getData();
                    Uri uri_cropped = Uri.fromFile(new File(getCacheDir(),"cropped"));
                    Crop.of(uri,uri_cropped).asSquare().start(this);

                }

            }else if(requestCode == Crop.REQUEST_CROP){
                final String studentID = preferences.getString("student_id","null");
                mStorage.child(email).putFile(Crop.getOutput(data));
                mStorage.child(email).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        final String Imageuri = uri.toString();
                        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("registered_users").child("students").child(studentID);
                        myRef.child("picture").setValue(Imageuri)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        Toast.makeText(admin_member_detail.this, "Profile image changed", Toast.LENGTH_SHORT).show();
                                    }
                                });


                    }
                });
            }
            else
            {
                Toast.makeText(admin_member_detail.this, "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        } catch(Exception e){
            Toast.makeText(admin_member_detail.this, e.toString(), Toast.LENGTH_LONG)
                    .show();

        }
    }
}

package com.example.codex;

public class Model_Book_Review {

    private String rating_id;
    private String user_id;
    private String book_isbn;
    private String comment;
    private int rating;

    public Model_Book_Review() {
    }

    public Model_Book_Review(String rating_id, String user_id, String book_isbn, String comment, int rating) {
        this.rating_id = rating_id;
        this.user_id = user_id;
        this.book_isbn = book_isbn;
        this.comment = comment;
        this.rating = rating;
    }

    public String getRating_id() {
        return rating_id;
    }

    public void setRating_id(String rating_id) {
        this.rating_id = rating_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getBook_isbn() {
        return book_isbn;
    }

    public void setBook_isbn(String book_isbn) {
        this.book_isbn = book_isbn;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}

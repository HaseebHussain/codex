package com.example.codex;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.QuickContactBadge;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nex3z.notificationbadge.NotificationBadge;

public class Admin_Panel extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    TextView mInquiriesCount ;

    SharedPreferences preferences;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin__panel);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Dashboard");

        preferences = getSharedPreferences("codex",MODE_PRIVATE);


        drawerLayout = findViewById(R.id.admin_drawer);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);



        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open,R.string.close);

        drawerLayout.addDrawerListener(actionBarDrawerToggle);

        actionBarDrawerToggle.syncState();
        NavigationView navigationView = findViewById(R.id.admin_nav);
        mInquiriesCount = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().
                findItem(R.id.userInquiries));
        mInquiriesCount.setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
        mInquiriesCount.setPadding(35,30,35,5);
//        mInquiriesCount.setBackground(getResources().getDrawable(R.drawable.notfication_background));
//        ViewGroup.LayoutParams params1 = mInquiriesCount.getLayoutParams();
//        params1.width = 10;
//        mInquiriesCount.setLayoutParams(params1);

        mInquiriesCount.setTextColor(getResources().getColor(R.color.colorPrimary));

//     getting inquiry count
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Inquiries");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                int number = 0;
                if(dataSnapshot.exists()){
                    for(DataSnapshot d :  dataSnapshot.getChildren()){
                        Model_Inquery inquery = d.getValue(Model_Inquery.class);
                        if(!inquery.isRead() && inquery.getReceiver().equals("Codex")){
                            number++;
                        }

                    }
                    mInquiriesCount.setText(""+number);
//                    mInquiriesCount.applyCount(String.valueOf(number));
                }else{
                    mInquiriesCount.setText(""+0);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment = null;
                Class fragmentClass;

                if(R.id.bookReservations == item.getItemId()){
                    toolbar.setTitle("Book Reservations");
                   Intent intent = new Intent(Admin_Panel.this,Reservation_list.class);
                   startActivity(intent);
                }else if(R.id.addBook == item.getItemId()){
                    toolbar.setTitle("Add Book");
                    GoToAddBook();
                }else if(R.id.Books == item.getItemId()){
                    toolbar.setTitle("Books");
                    Intent intent = new Intent(Admin_Panel.this, Search_Books.class);
                    startActivity(intent);
                }else if(R.id.approved == item.getItemId()){
                    toolbar.setTitle("Approved Books");
                    Intent intent = new Intent(Admin_Panel.this, approved_books.class);
                    startActivity(intent);
                }else if(R.id.userInquiries == item.getItemId()){
                    toolbar.setTitle("User Inquiries");
                    Intent intent = new Intent(Admin_Panel.this, Admin_Inqueries.class);
                    startActivity(intent);
                }else if(R.id.bookIssue == item.getItemId()){
                    toolbar.setTitle("Issued Books");
                    fragmentClass = bookIssue.class;

                    try {
                        fragment = (Fragment) fragmentClass.newInstance();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }else if(R.id.codex_panel == item.getItemId()){
                    toolbar.setTitle("Dashboard");
                    fragmentClass = fragmentAdminPanel.class;

                    try {
                        fragment = (Fragment) fragmentClass.newInstance();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }else if(R.id.UsersList == item.getItemId()){
                    toolbar.setTitle("Users List");
                    fragmentClass = FragmentUserList.class;

                    try {
                        fragment = (Fragment) fragmentClass.newInstance();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                if(fragment != null){
                    // Insert the fragment by replacing any existing fragment
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

                    // Highlight the selected item has been done by NavigationView
//                item.setChecked(true);

                    // Set action bar title
                    setTitle(item.getTitle());
                    // Close the navigation drawer
                    drawerLayout.closeDrawers();
                }



                return true;
            }
        });

        Fragment fragment = null;
        Class fragmentClass;
        fragmentClass = fragmentAdminPanel.class;

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        // Highlight the selected item has been done by NavigationView
//        item.setChecked(true);
        // Set action bar title
//        setTitle(item.getTitle());
        // Close the navigation drawer
        drawerLayout.closeDrawers();



    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.admin_panel,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent = new Intent(this, Login.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        FirebaseAuth.getInstance().signOut();
        startActivity(intent);
        return true;
    }

    public void GoToAddUser(View view) {
        Toast.makeText(this, "I am clicked", Toast.LENGTH_SHORT).show();
    }

    public void GoToAddBook(){
        new MaterialAlertDialogBuilder(this)
                .setTitle("Select")
                .setMessage("How to add book information?")
                .setPositiveButton("QR Code Scanner", new DialogInterface.OnClickListener() {
                @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(Admin_Panel.this,scan_book.class);
                        startActivity(i);
                    }
                }).setNegativeButton("Add Book Info Manually", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Admin_Panel.this, Add_New_Book.class);
                        startActivity(intent);
                    }
                })
                .show();
    }


    @Override
    public void onBackPressed() {
     //
//        if(category.equals("admin")){
//            Intent intent = new Intent(this, Admin_Panel.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//        }else{
            // Use the Builder class for convenient dialog construction
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Exit From Codex?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            Intent i = new Intent();
                            i.setAction(Intent.ACTION_MAIN);
                            i.addCategory(Intent.CATEGORY_HOME);
                            startActivity(i);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            dialog.dismiss();
                        }
                    });

            builder.show();
//        }

    }
}

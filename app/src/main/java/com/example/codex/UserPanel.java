package com.example.codex;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import de.hdodenhof.circleimageview.CircleImageView;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import androidx.appcompat.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Calendar;

public class UserPanel extends AppCompatActivity {

    DrawerLayout drawerLayout;
    String user;
    private ImageView imageView;
    CircleImageView imgProfileImage;
    private String studentID,email;
    Fragment fragment = null;
    Class fragmentClass;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_panel);

        fragmentClass = FragmentUserHome.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();

            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.fragment_user_panel,fragment).commit();

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        preferences = getSharedPreferences("codex",MODE_PRIVATE);
        editor = getSharedPreferences("codex",MODE_PRIVATE).edit();

        drawerLayout = findViewById(R.id.drawable_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        View navHeaderview = navigationView.getHeaderView(0);
        TextView tvUserName = navHeaderview.findViewById(R.id.tvUserName);
        imgProfileImage = navHeaderview.findViewById(R.id.imgProfile);
        String userfullname = preferences.getString("fullname","null");
        tvUserName.setText(userfullname);
        final String student_id = preferences.getString("student_id","null");

        studentID = preferences.getString("student_id","Null");
        email = preferences.getString("email","NULL");

        imgProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog profileDialog = new Dialog(UserPanel.this);
                profileDialog.setTitle("Modify Profile Image");
                profileDialog.setContentView(R.layout.layout_profile_image);
                imageView = profileDialog.findViewById(R.id.imgUserProfile);
                Drawable d =  imgProfileImage.getDrawable();
                imageView.setImageDrawable(d);
                MaterialButton changeImage = profileDialog.findViewById(R.id.btnChangeImage);
                MaterialButton close = profileDialog.findViewById(R.id.btnClose);
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        profileDialog.dismiss();
                        loadProfileImage();

                    }
                });
                changeImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        OpenGallery();
                    }
                });
                profileDialog.show();
            }
        });



        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open,R.string.close);

//        drawerLayout.addDrawerListener(actionBarDrawerToggle);

       user =  preferences.getString("category","null");


        if(user.equals("student")){
            drawerLayout.addDrawerListener(actionBarDrawerToggle);
            actionBarDrawerToggle.syncState();
        }

        NavigationView nv = findViewById(R.id.nav_view);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                Intent intent;


                switch (id){
                    case R.id.home:
                        fragmentClass = FragmentUserHome.class;
                        try {
                            fragment = (Fragment) fragmentClass.newInstance();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InstantiationException e) {
                            e.printStackTrace();
                        }

                        break;
                    case R.id.account:
                        intent = new Intent(UserPanel.this,admin_member_detail.class);
                        intent.putExtra("student_id",student_id);
                        startActivity(intent);
                        break;
                    case R.id.my_reservation_requests:
                        intent = new Intent(UserPanel.this,my_reservations.class);
                        startActivity(intent);
                        break;
                    case R.id.inquiry:
                        intent = new Intent(UserPanel.this,user_inquery.class);
                        startActivity(intent);
                        break;
                    case R.id.logout:
                        Logout();
                        break;
                    case R.id.issued_books:
                        fragmentClass = MyIssuedBooks.class;
                        try {
                            fragment = (Fragment) fragmentClass.newInstance();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InstantiationException e) {
                            e.printStackTrace();
                        }

                        break;
                    case R.id.previously_borrowed:
                        intent = new Intent(UserPanel.this,Previously_Borrowed_Books.class);
                        startActivity(intent);
                        break;
                    case R.id.hec:
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.digitallibrary.edu.pk"));
                        startActivity(browserIntent);

                }

                if(fragment != null){
                    // Insert the fragment by replacing any existing fragment
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.fragment_user_panel, fragment).commit();

                    // Highlight the selected item has been done by NavigationView
//                item.setChecked(true);

                    // Set action bar title
                    setTitle(item.getTitle());
                    // Close the navigation drawer
                    drawerLayout.closeDrawers();
                }

                return true;



            }
        });

        loadProfileImage();

        Intent intent = new Intent(this,DueDateService.class);
        Log.i("TAG", "onCreate: Username = "+userfullname);
        intent.putExtra("username",userfullname);
//        Start service
//        startService(intent);
        Calendar cal = Calendar.getInstance();
        PendingIntent pintent = PendingIntent
                .getService(this, 0, intent, 0);

        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        // Start service every hour
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                300000, pintent);

    }

    public void loadProfileImage(){
      try{
          //        Getting user image
          DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("registered_users").child("students").child(studentID);

          myRef.addValueEventListener(new ValueEventListener() {
              @Override
              public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                  if(dataSnapshot.exists()){
                      String profileImage = dataSnapshot.child("picture").getValue(String.class);

                      if(profileImage != "" && profileImage != null){
                          Picasso.get().load(profileImage).noFade().into(imgProfileImage);
                      }
                  }
              }

              @Override
              public void onCancelled(@NonNull DatabaseError databaseError) {
                  Toast.makeText(UserPanel.this, databaseError.toString(), Toast.LENGTH_SHORT).show();
              }
          });
      }catch (Exception e){
          e.printStackTrace();
      }
    }

    private void OpenGallery(){
//        ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.ACCESS_NETWORK_STATE},0);
        // Create intent to Open Image applications like Gallery, Google Photos
//        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        // Start the Intent
//        startActivityForResult(Intent.createChooser(galleryIntent,"Select Image from Gallery"),1);
        Crop.pickImage(this);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Uri uri = null;
            StorageReference mStorage = FirebaseStorage.getInstance().getReference("profile_images");
            // When an Image is picked
            if (requestCode == Crop.REQUEST_PICK && resultCode == Activity.RESULT_OK) {
                {
                     uri = data.getData();
                    Uri uri_cropped = Uri.fromFile(new File(getCacheDir(),"cropped"));
                    Crop.of(uri,uri_cropped).asSquare().start(this);
                    imageView.setImageURI(Crop.getOutput(data));
                    imgProfileImage.setImageURI(uri);

                }

            }else if(requestCode == Crop.REQUEST_CROP){
                imageView.setImageURI(Crop.getOutput(data));
                    mStorage.child(email).putFile(Crop.getOutput(data));
                    mStorage.child(email).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            final String Imageuri = uri.toString();
                            DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("registered_users").child("students").child(studentID);
                            myRef.child("picture").setValue(Imageuri)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            loadProfileImage();
                                            Toast.makeText(UserPanel.this, "Profile image changed", Toast.LENGTH_SHORT).show();
                                        }
                                    });


                        }
                    });
            }
            else
            {
                Toast.makeText(UserPanel.this, "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        } catch(Exception e){
            Toast.makeText(UserPanel.this, e.toString(), Toast.LENGTH_LONG)
                    .show();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.seach_menu,menu);
        MenuItem search= menu.findItem(R.id.app_bar_search);

        search.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                Intent intent = new Intent(UserPanel.this,Search_Books.class);
                startActivity(intent);
                return true;
            }
        });
        return true;
    }

    public void Logout(){
        Intent intent = new Intent(this, Login.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        editor.clear().apply();
        FirebaseAuth.getInstance().signOut();
        startActivity(intent);
    }

}

package com.example.codex;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

public class splash_screen extends AppCompatActivity {

    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        preferences = getSharedPreferences("codex",MODE_PRIVATE);
        final String category = preferences.getString("category","null");

        Log.i("TAG", "Splash Screen : "+category);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(category.equals("admin")){
                    Intent intent = new Intent(splash_screen.this, Admin_Panel.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fadein, R.anim.fade_out);
                }else if(category.equals("student")){

                    Intent intent = new Intent(splash_screen.this, UserPanel.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fadein, R.anim.fade_out);

                }else{
                    Intent intent = new Intent(splash_screen.this, Login.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fadein, R.anim.fade_out);
                }

            }

        },3000);
    }
}

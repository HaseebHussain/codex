package com.example.codex;


public class Model_Register_User {

    private String studentid;
    private String username;
    private String password;
    private String email;
    private String profileImage;

    public Model_Register_User() {
    }

    public Model_Register_User(String studentid, String username, String password, String email, String profileImage) {
        this.studentid = studentid;
        this.username = username;
        this.password = password;
        this.email = email;
        this.profileImage = profileImage;
    }

    public String getStudentid() {
        return studentid;
    }

    public void setStudentid(String studentid) {
        this.studentid = studentid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }
}
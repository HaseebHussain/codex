package com.example.codex;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.content.Context.MODE_PRIVATE;

public class FragmentCommentsEdit extends Fragment {

    FloatingActionButton mAddReview;
    DatabaseReference mRef;
    ProgressDialog progressDialog;
    private String student_id;
    private String isbn,category;
    List<Model_Book_Review> ReviewList;
    private RecyclerView mRecycleView;
    SharedPreferences preferences;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_comments_edit,container,false);

        mRef = FirebaseDatabase.getInstance().getReference("reviews");
        mAddReview = v.findViewById(R.id.addReview);
        progressDialog = new ProgressDialog(getContext());
        ReviewList = new ArrayList<>();
        mRecycleView = v.findViewById(R.id.ReviewList);
        preferences = getActivity().getSharedPreferences("codex",MODE_PRIVATE);

        student_id =  preferences.getString("university_id", "Null");
        isbn = getActivity().getIntent().getExtras().getString("isbn");
        category = preferences.getString("category","null");





//      checking if user already give review
        Query query1 = mRef.orderByChild("user_id").equalTo(student_id);
        query1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){
                    for (DataSnapshot d: dataSnapshot.getChildren()){
                        Model_Book_Review review = d.getValue(Model_Book_Review.class);
                        String getisbn = review.getBook_isbn();
                        String getuserId = review.getUser_id();
                        if(getisbn.equals(isbn) && getuserId.equals(student_id)){
                            mAddReview.setVisibility(View.GONE);
                        }
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

//        Load Book Reviews
        Query query2 = mRef.orderByChild("book_isbn").equalTo(isbn);
        query2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ReviewList.clear();
                if(dataSnapshot.exists()){

                    for(DataSnapshot d: dataSnapshot.getChildren()){
                        Model_Book_Review review = d.getValue(Model_Book_Review.class);
                        ReviewList.add(review);
                    }

                    Adapter_Book_Reviews adapter = new Adapter_Book_Reviews(ReviewList,getContext());
                    mRecycleView.setAdapter(adapter);
                    LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
                    mRecycleView.setLayoutManager(manager);


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        return v;

    }
}

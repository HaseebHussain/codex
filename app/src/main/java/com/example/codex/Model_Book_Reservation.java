package com.example.codex;

public class Model_Book_Reservation {

    String image_url;
    String student_name;
    String book_title;
    String request_status;

    public Model_Book_Reservation(String image_url, String student_name, String book_title, String request_status) {
        this.image_url = image_url;
        this.student_name = student_name;
        this.book_title = book_title;
        this.request_status = request_status;
    }

    public String getImage_url() {
        return image_url;
    }

    public String getStudent_name() {
        return student_name;
    }

    public String getBook_title() {
        return book_title;
    }

    public String getRequest_status() {
        return request_status;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public void setBook_title(String book_title) {
        this.book_title = book_title;
    }

    public void setRequest_status(String request_status) {
        this.request_status = request_status;
    }
}

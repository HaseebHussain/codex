package com.example.codex;

public class Model_Book_Return {

    private String issue_id;
    private String return_id;
    private String user_id;
    private String book_isbn;
    private String return_date;
    private String due_date;
    private int overdue_days;
    private Double fine;
    private boolean fine_collected;

    public Model_Book_Return() {
    }

    public Model_Book_Return(String issue_id, String return_id, String user_id, String book_isbn, String return_date, String due_date, int overdue_days, Double fine, boolean fine_collected) {
        this.issue_id = issue_id;
        this.return_id = return_id;
        this.user_id = user_id;
        this.book_isbn = book_isbn;
        this.return_date = return_date;
        this.due_date = due_date;
        this.overdue_days = overdue_days;
        this.fine = fine;
        this.fine_collected = fine_collected;
    }

    public String getIssue_id() {
        return issue_id;
    }

    public String getReturn_id() {
        return return_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getBook_isbn() {
        return book_isbn;
    }

    public String getReturn_date() {
        return return_date;
    }

    public String getDue_date() {
        return due_date;
    }

    public Double getFine() {
        return fine;
    }

    public int getOverdue_days() {
        return overdue_days;
    }

    public boolean isFine_collected() {
        return fine_collected;
    }


}

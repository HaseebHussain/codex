package com.example.codex;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;


public class scan_book extends AppCompatActivity {


    public  Button  previewBtn, linkBtn,addBook;
    public static TextInputEditText authorText,mSubtitle,mCopies, mPublicationPlace,titleText,mEditor,mEdition,mVolume, mPublisher,mDescription,
            mBookDate,mPages,mSource,mDccNo,mAuthorMark,mCategories;
    public  TextView ratingCountText,mISBN,mEntryDate;
    public static String subject,author,title,description,date,rating,book_rating,bookLink,department,publisher;
    public static StringBuilder categories;
    public static Bitmap thumbnail;
    public  LinearLayout starLayout;
    public  ImageView thumbView;
    public static ImageView[] starViews;
    public static Bitmap thumbImg;
    public static String scanContent;
    private static DatabaseReference reference;
    public  ProgressBar progressBar;
    boolean progress = true;
    private static String Image_Uri = "";
    int[] bookAvailable = new int[]{0};
    public static Context context;
    StorageReference storageRef;
    public static Date dateInstance;
    public static SimpleDateFormat dateFormat;
    public static String currentDate;
    public static ScrollView mScrollView;
    public static int copies;
    AppCompatSpinner mSubjects,mLanguage,mBinding,mShelfNum,mShelfRow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_book);

        previewBtn = (Button)findViewById(R.id.preview_btn);
        previewBtn.setVisibility(View.GONE);
        linkBtn = (Button)findViewById(R.id.link_btn);
        linkBtn.setVisibility(View.GONE);
        Image_Uri = "";

        Toolbar myToolbar =  findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        progressBar = findViewById(R.id.progressBar2);

//        Databse Reference
        reference = FirebaseDatabase.getInstance().getReference("Book");


        storageRef =  FirebaseStorage.getInstance().getReference("Book_Images/");

//        LInk button
        linkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tag = (String)v.getTag();
                //launch the url
                Intent webIntent = new Intent(Intent.ACTION_VIEW);
                webIntent.setData(Uri.parse(tag));
                startActivity(webIntent);

            }
        });

        previewBtn.setVisibility(View.GONE);
//       PReview button
        previewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String tag = (String) v.getTag();
//                Intent intent = new Intent(scan_book.this, EmbeddedBook.class);
//                intent.putExtra("isbn", tag);
//                startActivity(intent);
            }
        });

        mScrollView = findViewById(R.id.scrollView2);
        authorText = findViewById(R.id.book_author);
        titleText = findViewById(R.id.book_title);
        mSubtitle = findViewById(R.id.subtitle);
        mEntryDate = findViewById(R.id.entry_date);
        mEditor = findViewById(R.id.editor);
        mEdition = findViewById(R.id.edition);
        mVolume = findViewById(R.id.volume);
        mPublicationPlace = findViewById(R.id.publication_place);
        mBookDate = findViewById(R.id.book_date);
        mPublisher = findViewById(R.id.publisher);
        mPages = findViewById(R.id.pages);
        mSource = findViewById(R.id.source);
        mDccNo = findViewById(R.id.dcc_no);
        mAuthorMark = findViewById(R.id.author_mark);
        mCategories = findViewById(R.id.txt_categories);
        mDescription = findViewById(R.id.book_description);
        mCopies = findViewById(R.id.copies);
        mSubjects = findViewById(R.id.subject);
        mLanguage = findViewById(R.id.language);
        mBinding = findViewById(R.id.binding);
        mShelfNum = findViewById(R.id.shelf_no);
        mShelfRow = findViewById(R.id.shelf_row);



//        descriptionText = findViewById(R.id.book_description);
//        dateText = findViewById(R.id.book_date);
        starLayout = (LinearLayout)findViewById(R.id.star_layout);
//        ratingCountText = (TextView)findViewById(R.id.book_rating_count);
        thumbView = findViewById(R.id.thumb);
//        txtCategories = findViewById(R.id.txt_categories);
//        msp_department = findViewById(R.id.spinner);
//        spinner_layout = findViewById(R.id.spinner_layout);
        addBook = findViewById(R.id.add_book);
        context = this;
        mISBN = findViewById(R.id.isbn);
        dateInstance = Calendar.getInstance().getTime();
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        currentDate = dateFormat.format(dateInstance);

        mEntryDate.setText(currentDate);
        mEdition.setText("Null");
        mEditor.setText("Null");
        mVolume.setText("Null");
        mSource.setText("Null");
        mDccNo.setText("Null");
        mAuthorMark.setText("Null");
        mCopies.setText("0");
        copies = Integer.parseInt(mCopies.getText().toString());
        mPublicationPlace.setText("Null");


        // Create a storage reference from our app
//        storageRef = FirebaseStorage.getInstance().getReference().child("Book_Images");

//        Setting spinner values
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
//                R.array.department, android.R.layout.simple_spinner_item);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        msp_department.setAdapter(adapter);



        starViews = new ImageView[5];
        for(int s=0; s<starViews.length; s++){
            starViews[s]=new ImageView(this);
        }


        if (savedInstanceState != null){
//            authorText.setText(savedInstanceState.getString("author"));
//            titleText.setText(savedInstanceState.getString("title"));
//            descriptionText.setText(savedInstanceState.getString("description"));
//            dateText.setText(savedInstanceState.getString("date"));
//            ratingCountText.setText(savedInstanceState.getString("ratings"));
//            int numStars = savedInstanceState.getInt("stars");//zero if null
//            for(int s=0; s<numStars; s++){
//                starViews[s].setImageResource(R.drawable.star);
//                starLayout.addView(starViews[s]);
//            }
//            starLayout.setTag(numStars);
//            thumbImg = (Bitmap)savedInstanceState.getParcelable("thumbPic");
//            thumbView.setImageBitmap(thumbImg);
//            previewBtn.setTag(savedInstanceState.getString("isbn"));
//
//            if(savedInstanceState.getBoolean("isEmbed")) previewBtn.setEnabled(true);
//            else previewBtn.setEnabled(false);
//            if(savedInstanceState.getInt("isLink")==View.VISIBLE) linkBtn.setVisibility(View.VISIBLE);
//            else linkBtn.setVisibility(View.GONE);
//            previewBtn.setVisibility(View.VISIBLE);
        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.scan_menu,menu);
        return true;

    }

    private boolean requsetPermissoin(){
        if (ActivityCompat.checkSelfPermission(scan_book.this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Permission Already Granted", Toast.LENGTH_SHORT).show();
            return true;
        }
        else{
            ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.CAMERA},1);
            return false;
        }
        // Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //startActivityForResult(intent,0);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.scan_book:
                // User chose the "Settings" item, show the app settings UI...
                if(requsetPermissoin()){
                    IntentIntegrator scanIntegrator = new IntentIntegrator(scan_book.this);
                    scanIntegrator.initiateScan();
                }


                return true;
        }

        return false;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//retrieve scan result
        super.onActivityResult(requestCode, resultCode, intent);
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);

        if (scanningResult != null) {


//we have a result
            scanContent = scanningResult.getContents();
            //get format name of data scanned
            String scanFormat = scanningResult.getFormatName();
            previewBtn.setTag(scanContent);

            if (isNumeric(scanContent)) {
                String bookSearchString = "https://www.googleapis.com/books/v1/volumes?" +
                        "q=isbn="+scanContent+"&key=AIzaSyD1bz8GYNTTLTpJ7ee_rIRgfWFMazPMT04";
                mISBN.setText(scanContent);
//                titleText.setText(scanContent);
                Log.d("TAG", "Search Query => " + bookSearchString);
                new GetBookInfo().execute(bookSearchString);
            } else {
                Toast.makeText(this, "ISBN number is not found", Toast.LENGTH_SHORT).show();
            }


        } else {
            Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT).show();
        }

    }

    private boolean isNumeric(String scanContent) {

        //Checking the qrcode is a isbn number not a link

            try {
                double d = Double.parseDouble(scanContent);
                return true;
            } catch (NumberFormatException | NullPointerException nfe) {
                return false;
            }
    }


    class GetBookInfo extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(String result) {
//parse search results
            try{
//parse results
                previewBtn.setVisibility(View.VISIBLE);
                mScrollView.setVisibility(View.VISIBLE);
                JSONObject resultObject = new JSONObject(result);
                JSONArray bookArray = resultObject.getJSONArray("items");

                JSONObject bookObject = bookArray.getJSONObject(0);
                JSONObject volumeObject = bookObject.getJSONObject("volumeInfo");
                Log.i("TAG", "onPostExecute: Volume Info "+volumeObject);

                try{
                    titleText.setText(volumeObject.getString("title"));
                    title = volumeObject.getString("title");
                }catch (JSONException e){
                    titleText.setText("Null");
                    Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                }

//                subtitle{
                try{
                    mSubtitle.setText(volumeObject.getString("subtitle"));
                }catch (JSONException e){
                    mSubtitle.setText("Null");
                    Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                }
                try{
                    mPublisher.setText(volumeObject.getString("publisher"));
                }catch (JSONException e){
                    mPublisher.setText("Null");
                    Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                }

                try{
                    mPages.setText(volumeObject.getString("pageCount"));
                }catch (JSONException e){
                    mPages.setText("Null");
                    Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                }

                try{
//                    for authors
                    StringBuilder authorBuild = new StringBuilder("");
                    JSONArray authorArray = volumeObject.getJSONArray("authors");
                    for(int a=0; a<authorArray.length(); a++){
                        if(a>0) authorBuild.append(", ");
                        authorBuild.append(authorArray.getString(a));
                    }
                    authorText.setText(authorBuild.toString());
//                    author = authorBuild.toString();


                }
                catch(JSONException jse){
                    authorText.setText("Null");
                    Log.i("TAG", jse.toString());
                }


                //                for publisher
//                try{
//                    publisher = volumeObject.getString("publisher");
//                }catch (JSONException e){
//                    publisher = "";
//                }
//                if(volumeObject.has("publisher")){
//                    publisher = volumeObject.getString("publisher");
//                }else{
//                }


// calling the image url

                 if(volumeObject.has("imageLinks")){
                     String imageInfo = volumeObject.getJSONObject("imageLinks").getString("thumbnail");
//                     Log.i("TAG", "Image URL => "+imageInfo.);
//                    new GetBookThumb().execute(imageInfo.toString());
                     if(imageInfo != null && !imageInfo.equals("")){
                         Picasso.get().load(imageInfo).into(thumbView);
                     }else{
                        thumbView.setImageDrawable(getResources().getDrawable(R.drawable.book));
                     }
                 }else{
                     Log.i("TAG", "Image URL =>  NOT FOUND");
                 }





//
////                For publisher
                try{ mBookDate.setText(volumeObject.getString("publishedDate"));
                    date = volumeObject.getString("publishedDate");
                }
                catch(JSONException jse){
                    mBookDate.setText("Null");
                    jse.printStackTrace();
                }

//                For description
                try{ mDescription.setText(volumeObject.getString("description"));
                    description = volumeObject.getString("description");
                }
                catch(JSONException jse){
                    mDescription.setText("Null");
                    jse.printStackTrace();
                }
//
//                for catgory
                try {
                    categories = new StringBuilder();
                    JSONArray arrayCat = volumeObject.getJSONArray("categories");
                    for(int i = 0; i < arrayCat.length(); i++){
                        if(i>0) categories.append(", ");
                        categories.append(arrayCat.getString(i));
                    }
                    mCategories.setText(categories);


                }catch (JSONException e){

                }

//                For stars
                try{
//set stars
                    double decNumStars = Double.parseDouble(volumeObject.getString("averageRating"));
                    book_rating = volumeObject.getString("averageRating");
                    int numStars = (int)decNumStars;
                    starLayout.setTag(numStars);
                    starLayout.removeAllViews();
                    for(int s=0; s<numStars; s++){
                        starViews[s].setImageResource(R.drawable.star);
                        starLayout.addView(starViews[s]);
                    }
//                    try{ ratingCountText.setText(" - "+volumeObject.getString("ratingsCount")+" ratings"); }
//                    catch(JSONException jse){
//                        ratingCountText.setText("");
//                        jse.printStackTrace();
//                    }

//                  Book preview
                    try{
                        boolean isEmbeddable = Boolean.parseBoolean
                                (bookObject.getJSONObject("accessInfo").getString("embeddable"));
                        if(isEmbeddable) previewBtn.setEnabled(true);
                        else previewBtn.setEnabled(false);
                    }
                    catch(JSONException jse){
                        previewBtn.setEnabled(false);
                        jse.printStackTrace();
                    }

//                    information link
                    try{
                        linkBtn.setTag(volumeObject.getString("infoLink"));
                        linkBtn.setVisibility(View.VISIBLE);
                        bookLink = volumeObject.getString("infoLink");

                    }
                    catch(JSONException jse){
                        linkBtn.setVisibility(View.GONE);
                        jse.printStackTrace();
                    }



                }
                catch(JSONException jse){
                    starLayout.removeAllViews();
                    Toast.makeText(context, jse.toString(), Toast.LENGTH_SHORT).show();
                }



//               setting department spinner visible
//                spinner_layout.setVisibility(View.VISIBLE);
            }
            catch (Exception e) {
//no result


                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setMessage("Data not found");
                alertDialogBuilder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                e.printStackTrace();
                Log.i("TAG", "onPostExecute: "+e.toString());
//                mISBN.setText("NOT FOUND");
//                authorText.setText("");
//                descriptionText.setText("");
//                dateText.setText("");
//                starLayout.removeAllViews();
////                ratingCountText.setText("");
//                thumbView.setImageBitmap(null);
//                previewBtn.setVisibility(View.GONE);
//                progressBar.setVisibility(View.GONE);
            }


            progressBar.setVisibility(View.GONE);
            addBook.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... bookURLs) {
            StringBuilder bookBuilder = new StringBuilder();



            for (String bookSearchURL : bookURLs) {
//search urls

                try {
                    //get the data
                    HttpClient bookClient = new DefaultHttpClient();
                    HttpGet bookGet = new HttpGet(bookSearchURL);
                    HttpResponse bookResponse = bookClient.execute(bookGet);

                    StatusLine bookSearchStatus = bookResponse.getStatusLine();
                    if (bookSearchStatus.getStatusCode()==200) {
                        //we have a result
                        HttpEntity bookEntity = bookResponse.getEntity();
                        InputStream bookContent = bookEntity.getContent();
                        InputStreamReader bookInput = new InputStreamReader(bookContent);
                        BufferedReader bookReader = new BufferedReader(bookInput);
                        String lineIn;
                        while ((lineIn=bookReader.readLine())!=null) {
                            bookBuilder.append(lineIn);
                        }

                    }

                }
                catch(Exception e){ e.printStackTrace(); }
            }
            return bookBuilder.toString();

        }
//fetch book info
    }



    protected void onSaveInstanceState(@NotNull Bundle savedBundle) {
        super.onSaveInstanceState(savedBundle);
//        savedBundle.putString("title", "" + titleText.getText());
//        savedBundle.putString("author", "" + authorText.getText());
//        savedBundle.putString("description", "" + descriptionText.getText());
//        savedBundle.putString("date", "" + dateText.getText());
//        savedBundle.putString("ratings", "" + ratingCountText.getText());
//        savedBundle.putParcelable("thumbPic", thumbImg);
//        if (starLayout.getTag() != null)
//            savedBundle.putInt("stars", Integer.parseInt(starLayout.getTag().toString()));
//        savedBundle.putBoolean("isEmbed", previewBtn.isEnabled());
//        savedBundle.putInt("isLink", linkBtn.getVisibility());
//        if (previewBtn.getTag() != null)
//            savedBundle.putString("isbn", previewBtn.getTag().toString());
//
//

    }

    //    Add the book to firebase database
    public void add_book(View view){

        progressBar.setVisibility(View.VISIBLE);
        DatabaseReference mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        final Query query = mFirebaseDatabaseReference.child("Book").orderByChild("isbn").equalTo(scanContent);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                   if(dataSnapshot.exists()){
                       Toast.makeText(scan_book.this, "Book already exist", Toast.LENGTH_SHORT).show();
                       progressBar.setVisibility(View.GONE);
                       query.removeEventListener(this);
                   }else{
                       AddBookDetails();
                   }


            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(scan_book.this, databaseError.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }



    public void AddBookDetails() {


        // Get the data from an ImageView as bytes
        thumbView.setDrawingCacheEnabled(true);
        thumbView.buildDrawingCache();
        Bitmap bitmap = ((BitmapDrawable) thumbView.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = storageRef.child(scanContent).putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                // ...
                subject = mSubjects.getSelectedItem().toString();
                Image_Uri = taskSnapshot.getDownloadUrl().toString();
                publisher = mPublisher.getText().toString();
                String authors = authorText.getText().toString();
                String category = mCategories.getText().toString();
                String description = mDescription.getText().toString();
                String entry_date = mEntryDate.getText().toString();
                String subject = mSubjects.getSelectedItem().toString();
                String subtitle = mSubtitle.getText().toString();
                String editor = mEditor.getText().toString();
                String edition = mEdition.getText().toString();
                String volume = mVolume.getText().toString();
                String language = mLanguage.getSelectedItem().toString();
                String pages = mPages.getText().toString();
                String source = mSource.getText().toString();
                String dcc = mDccNo.getText().toString();
                String authorMark = mAuthorMark.getText().toString();
                String binding = mBinding.getSelectedItem().toString();
                int shelfnum = Integer.parseInt(mShelfNum.getSelectedItem().toString());
                int shelfrow = Integer.parseInt(mShelfRow.getSelectedItem().toString());
                String pubPlace = mPublicationPlace.getText().toString();
                String pubDate = mBookDate.getText().toString();


                Log.d("TAG", "Download URL => " + Image_Uri);

//                    Model_Book_Details model_book_details = new Model_Book_Details(scanContent,title,author,"","Available",3,
//                            department,Image_Uri,publisher,categories.toString(),description,true);
                Model_Book_Details model_book_details = new Model_Book_Details(scanContent, title, authors, edition
                        , "Available",copies,Image_Uri,publisher,category,description,false,entry_date,subject,subtitle,editor,volume,pubPlace,pubDate,language,pages,source,authorMark,binding,shelfnum,shelfrow,0,"null");
                final String id = reference.push().getKey();
                reference = FirebaseDatabase.getInstance().getReference("Book").child("-H" + scanContent);
                reference.setValue(model_book_details).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(scan_book.this, "Book Added Successfully", Toast.LENGTH_SHORT).show();
                        finish();
                        startActivity(getIntent());

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(scan_book.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

}


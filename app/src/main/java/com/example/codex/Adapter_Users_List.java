package com.example.codex;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.PipedInputStream;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class Adapter_Users_List extends RecyclerView.Adapter<Adapter_Users_List.ViewHolder> {

    private Context mContext;
    private List<Model_Student_Detail> mList;

    public Adapter_Users_List(Context mContext, List<Model_Student_Detail> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.layout_users_list, parent, false);
        return new Adapter_Users_List.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final Model_Student_Detail student_details = mList.get(position);

        holder.mDepartment.setText(student_details.getDepartment());
        holder.mUsername.setText(student_details.getName());
        if(!student_details.getPicture().equals("")){
            Picasso.get().load(student_details.getPicture()).into(holder.mProfileImage);
        }

        holder.mMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               try{

                   Intent intent = new Intent(mContext,admin_member_detail.class);
                   intent.putExtra("student_id",student_details.getStudent_id());
                   mContext.startActivity(intent);

               }catch (Exception e){
                   e.printStackTrace();
               }
            }
        });



    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView mProfileImage;
        TextView mUsername,mDepartment;
        LinearLayout mMainLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mProfileImage = itemView.findViewById(R.id.ProfileImage);
            mUsername = itemView.findViewById(R.id.Username);
            mDepartment = itemView.findViewById(R.id.Department);
            mMainLayout = itemView.findViewById(R.id.main_layout);

        }
    }
}

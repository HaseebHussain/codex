package com.example.codex;

import android.Manifest;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.github.clans.fab.FloatingActionButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Random;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import static android.content.Context.DOWNLOAD_SERVICE;

public class fregBookDetail extends Fragment {

    private TextView mTextView_Title,mTextView_Writer,mTextView_Edition,mTextView_Status,mTextView_Copies,mTvDescription;
    private ImageView mImg_Book;
    private DatabaseReference mDatabase;
    private String isbn;
    private String Date;
    private String instance ;
    SharedPreferences preferences;
    private ConstraintLayout mMainLayout;
    private ProgressDialog progressDialog;
    private long downloadID;




    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.freg_book_detail,container,false);
        setHasOptionsMenu(true);
        mImg_Book = v.findViewById(R.id.img_book);
        mTextView_Title = v.findViewById(R.id.tv_title);
        mTextView_Writer = v.findViewById(R.id.tv_writer);
        mTextView_Edition = v.findViewById(R.id.tv_edition);
        mTextView_Status  = v.findViewById(R.id.tv_status);
        mTextView_Copies = v.findViewById(R.id.tv_copies);
        final Button mBtn_Reserve_Book = v.findViewById(R.id.btn_reserve_book);
        final Button mDownladPdf = v.findViewById(R.id.download_pdf);

        mTvDescription = v.findViewById(R.id.tvDescription);
        final FloatingActionButton mBtnAlert = v.findViewById(R.id.btnSetAlert);
        FloatingActionButton  mBtnEdit = v.findViewById(R.id.btnBookEdit);
        FloatingActionButton  mBtnDelete = v.findViewById(R.id.btnBookDelete);
        preferences = getActivity().getSharedPreferences("codex", Context.MODE_PRIVATE);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please Wait...!");

        mMainLayout = v.findViewById(R.id.MainLayout);

        String user = preferences.getString("category", "Not Found");
        final String student_id = preferences.getString("student_id","null");

        if(user.equals("student")){
            mBtnEdit.setVisibility(View.GONE);
            mBtnDelete.setVisibility(View.GONE);
        }else{
            mBtnAlert.setVisibility(View.GONE);
        }

        getContext().registerReceiver(onDownloadComplete,new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        DatabaseReference mDbRef = FirebaseDatabase.getInstance().getReference("Book");


        //        firbase
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Reservations");
        isbn = getActivity().getIntent().getStringExtra("isbn");

        Query book_detail = mDbRef.orderByChild("isbn").equalTo(isbn);
        book_detail.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    for(DataSnapshot d : dataSnapshot.getChildren()){
                        Model_Book_Details model_book_details = d.getValue(Model_Book_Details.class);
                        if(!model_book_details.getName().equals("")){
                            preferences.edit().putString("book_name",model_book_details.getName()).apply();
                            preferences.edit().putString("pdf_uri",model_book_details.getPdf()).apply();
                            mTextView_Title.setText(model_book_details.getName());
                            mTextView_Writer.setText(model_book_details.getWriter());
                            mTextView_Edition.setText(model_book_details.getEdition());
                            mTextView_Status.setText(model_book_details.getStatus());
                            mTextView_Copies.setText(String.valueOf(model_book_details.getCopies()));
                            if(model_book_details.getCopies() <= 0){
                                mBtn_Reserve_Book.setVisibility(View.GONE);
                                mBtnAlert.setVisibility(View.VISIBLE);
                                Toast.makeText(getContext(), "This Book is not available! Click alert button to get notified if available", Toast.LENGTH_LONG).show();
                            }
                            Picasso.get()
                                    .load(model_book_details.getImageUri())
                                    .placeholder(R.mipmap.ic_launcher)
                                    .into(mImg_Book);
                            mTvDescription.setText(model_book_details.getDescription());
                        }

                    }
                }else{
                    Toast.makeText(getContext(), "Data Not found", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onCancelled(@NotNull DatabaseError databaseError) {
                Toast.makeText(getContext(), databaseError.toString(), Toast.LENGTH_SHORT).show();

            }
        });



//        Button click listener
        mBtn_Reserve_Book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reserveBook();
            }
        });

        mDownladPdf.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
               try{
                   ConnectionCheck connection = new ConnectionCheck(getContext());
                   if(connection.RequestInternetConnection()){
                       if(isStoragePermissionGranted()){
                           beginDownload();
                       }

                   }else{
                       Toast.makeText(getContext(), "Please check the connection", Toast.LENGTH_SHORT).show();
                   }
               }catch (Exception e){
                   Toast.makeText(getContext(),e.toString(), Toast.LENGTH_SHORT).show();
               }
            }
        });

//        Floating button onclick listner
        mBtnAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("book_alerts");
                String alertId = myRef.push().getKey();
                String BookName = preferences.getString("book_name","null");
                String deviceInstance = preferences.getString("device_instance","null");
                Model_Book_Alert alert = new Model_Book_Alert(alertId,student_id,isbn,BookName,deviceInstance);
                if (alertId != null) {
                    myRef.child(alertId).setValue(alert)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    Snackbar.make(mMainLayout, "Alert Enabled for this book", Snackbar.LENGTH_SHORT)
                                            .show();
                                }
                            });
                }

            }
        });


//        Edit button
        mBtnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Edit is Clicked", Toast.LENGTH_SHORT).show();
            }
        });

        mBtnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Query q = FirebaseDatabase.getInstance().getReference("Book");
                q.orderByChild("isbn").equalTo(isbn).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                for(DataSnapshot d: dataSnapshot.getChildren()){
                                    d.getRef().removeValue();
                                    Toast.makeText(getContext(), "Book Deleted", Toast.LENGTH_SHORT).show();
                                    getActivity().finish();
                                }


                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
            }
        });


//        Check if book alert is enable already
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("book_alerts");
        Query q = myRef.orderByChild("student_id").equalTo(student_id);
                q.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            for(DataSnapshot d : dataSnapshot.getChildren()){
                                Model_Book_Alert alert = d.getValue(Model_Book_Alert.class);
                                String Bisbn = null;
                                if (alert != null) {
                                    Bisbn = alert.getBook_isbn();
                                }
                                String Sstudent_id = alert.getStudent_id();

                                if(Bisbn.equals(isbn) && Sstudent_id.equals(student_id)){
                                    mBtnAlert.setVisibility(View.GONE);
                                }
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


        return v;


    }

    public void reserveBook(){
        progressDialog.show();
        final String UserID = preferences.getString("student_id", "Student ID not Found");
        final String username =  preferences.getString("fullname", "Null");

        Date = new ConnectionCheck(getContext()).CurrentDateTime();

        final DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("Reservations");
        final Query query1 = myRef.orderByChild("book_ISBN").equalTo(isbn);
        final Query query2 = myRef.orderByChild("student_id").equalTo(UserID);

        query1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    query2.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                            if(dataSnapshot.exists()){

                                Toast.makeText(getContext(), "This book is already reserved", Toast.LENGTH_LONG).show();
                                progressDialog.dismiss();
                            }else{
//                                Toast.makeText(Book_Details.this, "This book is not reserved", Toast.LENGTH_LONG).show();

                                final DatabaseReference myref = FirebaseDatabase.getInstance().getReference("Book").child("-H"+isbn);
                                myref.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        final String imgURL;

                                        final String reserveId = mDatabase.push().getKey();

                                        imgURL = dataSnapshot.child("imageUri").getValue(String.class);
                                        int book_count = dataSnapshot.child("copies").getValue(Integer.class);
                                        instance = FirebaseInstanceId.getInstance().getToken();
                                        if(book_count > 0){
                                            myref.child("copies").setValue(book_count-1).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    Model_Reserve_Book reserve_book = new Model_Reserve_Book(reserveId,UserID,username,isbn,mTextView_Title.getText().toString(),imgURL,Date,"pending",instance);
                                                    mDatabase.child(reserveId).setValue(reserve_book);
                                                    progressDialog.dismiss();
                                                    Toast.makeText(getContext(), "Book Reserved Successfully", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }else{
                                            progressDialog.dismiss();
                                            Toast.makeText(getContext(), "Book is not available in Library! ", Toast.LENGTH_LONG).show();
                                        }


                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                            }
                        }

                        @Override
                        public void onCancelled(@NotNull DatabaseError databaseError) {

                        }
                    });
                }else{

                    final DatabaseReference myref = FirebaseDatabase.getInstance().getReference("Book").child("-H"+isbn);
                    myref.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            String imgURL = null;
                            final String reserveId = mDatabase.push().getKey();
                            imgURL = dataSnapshot.child("imageUri").getValue(String.class);

                            instance = FirebaseInstanceId.getInstance().getToken();

                            int book_count = dataSnapshot.child("copies").getValue(Integer.class);
                            instance = FirebaseInstanceId.getInstance().getToken();
                                        if(book_count > 0){
                                            final String finalImgURL = imgURL;
                                            myref.child("copies").setValue(book_count-1).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    Model_Reserve_Book reserve_book = new Model_Reserve_Book(reserveId,UserID,username,isbn,mTextView_Title.getText().toString(), finalImgURL,Date,"pending",instance);
                                                    mDatabase.child(reserveId).setValue(reserve_book);
                                                    progressDialog.dismiss();
                                                    Toast.makeText(getContext(), "Book Reserved Successfully", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }else{
                                            progressDialog.dismiss();
                                            Toast.makeText(getContext(), "Book is not available in Library! ", Toast.LENGTH_LONG).show();
                                        }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });


                }
            }

            @Override
            public void onCancelled(@NotNull DatabaseError databaseError) {

            }
        });




    }

    public int gen() {
        Random r = new Random( System.currentTimeMillis() );
        return 10000 + r.nextInt(20000);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void beginDownload(){

        String book_name = preferences.getString("book_name","null");
        String pdf_uri = preferences.getString("pdf_uri","null");


        if(!pdf_uri.equals("null")){
            File file=new File(getContext().getExternalFilesDir(null),book_name);
        /*
        Create a DownloadManager.Request with all the information necessary to start the download
         */
            DownloadManager.Request request=new DownloadManager.Request(Uri.parse(pdf_uri))
                    .setTitle(book_name)// Title of the Download Notification
                    .setDescription("Downloaded from Codex")// Description of the Download Notification
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)// Visibility of the download Notification
                    .setDestinationUri(Uri.fromFile(file))// Uri of the destination file// Set if charging is required to begin the download
                    .setAllowedOverMetered(true)// Set if download is allowed on Mobile network
                    .setAllowedOverRoaming(true);// Set if download is allowed on roaming network
            DownloadManager downloadManager= (DownloadManager) getContext().getSystemService(DOWNLOAD_SERVICE);
            downloadID = downloadManager.enqueue(request);// enqueue puts the download request in the queue.
        }else{
            Toast.makeText(getContext(), "Pdf not available", Toast.LENGTH_LONG).show();
        }
    }

    private BroadcastReceiver onDownloadComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //Fetching the download id received with the broadcast
            long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            //Checking if the received broadcast is for our enqueued download by matching download id
            if (downloadID == id) {
                Toast.makeText(getContext(), "Download Completed", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        getContext().unregisterReceiver(onDownloadComplete);
    }

    private  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getContext().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {

                return true;
            } else {

                Log.v("TAG","Permission is revoked");
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation

            return true;
        }
    }
}

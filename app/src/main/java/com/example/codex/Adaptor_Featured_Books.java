package com.example.codex;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class Adaptor_Featured_Books extends RecyclerView.Adapter<Adaptor_Featured_Books.ViewHolder> {

    private Context mContext;
    private List<Model_Book_Details> mList;


    Adaptor_Featured_Books(Context mContext, List<Model_Book_Details> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    class ViewHolder extends RecyclerView.ViewHolder{


        TextView mTv_Std_ID;
        TextView mTV_ISBN;
        ImageView mImg_Image;


        ViewHolder(View itemView) {
            super(itemView);
            mTv_Std_ID = itemView.findViewById(R.id.tv_title);
            mTV_ISBN = itemView.findViewById(R.id.tv_writer);
            mImg_Image = itemView.findViewById(R.id.img_book);

        }
    }

    @NonNull
    @Override
    public Adaptor_Featured_Books.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.layout_featured_books, parent, false);
        return new Adaptor_Featured_Books.ViewHolder(v);
    }



    @Override
    public void onBindViewHolder(@NonNull Adaptor_Featured_Books.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        Model_Book_Details uploadCurrent = mList.get(position);

        holder.mTv_Std_ID.setText(uploadCurrent.getName());
        holder.mTV_ISBN.setText(uploadCurrent.getWriter());

//        enable marque
        holder.mTv_Std_ID.setSelected(true);
        holder.mTV_ISBN.setSelected(true);

        Picasso.get().load(uploadCurrent.getImageUri()).into(holder.mImg_Image);


        holder.mImg_Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Model_Book_Details pos = mList.get(position);
                Intent intent = new Intent(mContext,bookDetails.class);
                intent.putExtra("isbn",pos.getIsbn());

                mContext.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }




}

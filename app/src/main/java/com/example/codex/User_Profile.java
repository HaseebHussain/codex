package com.example.codex;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import de.hdodenhof.circleimageview.CircleImageView;

public class User_Profile extends AppCompatActivity {

    TextView mId,mName,mAddress,mContact,mAcadmic,mDepart;
    String s;
    CircleImageView mprofileImage;
    Toolbar toolbar;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user__profile);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        mprofileImage = findViewById(R.id.profileImage);
        preferences = getSharedPreferences("codex",MODE_PRIVATE);
        mId = findViewById(R.id.tv_id);
        mName = findViewById(R.id.tv_name);
        mAddress = findViewById(R.id.tv_address);
        mContact = findViewById(R.id.tv_contact);
        mAcadmic = findViewById(R.id.tv_acadamic);
        mDepart = findViewById(R.id.tv_depart);





        final String UserID = preferences.getString("student_id", "NULL");
        if(new ConnectionCheck(this).RequestInternetConnection()){
            DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("registered_users").child("students").child(UserID);

            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        String profileImage = dataSnapshot.child("picture").getValue(String.class);
                        Picasso.get().load(profileImage).noFade().into(mprofileImage);
                        mId.setText(dataSnapshot.child("university_id").getValue(String.class));
                        mName.setText(dataSnapshot.child("name").getValue(String.class));
                        mAddress.setText(dataSnapshot.child("address").getValue(String.class));
                        mContact.setText(dataSnapshot.child("phone_no").getValue(String.class));
                        mAcadmic.setText(dataSnapshot.child("batch_year").getValue(String.class));
                        mDepart.setText(dataSnapshot.child("department").getValue(String.class));
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(User_Profile.this, databaseError.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }

    }
}
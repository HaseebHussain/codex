package com.example.codex;


public class Model_Book_Details {
    private String isbn;
    private String name;
    private String writer;
    private String edition;
    private String status;
    private int copies;
    private String ImageUri;
    private String publisher;
    private String categories;
    private String description;
    private boolean featured;
    private String entry_date;
    private String subject;
    private String subtitle;
    private String editor;
    private String volume;
    private String publication_place;
    private String publication_date;
    private String language;
    private String pages;
    private String dcc_number;
    private String author_mark;
    private String binding;
    private int shelf_number;
    private int shelf_row;
    private double rating;
    private String pdf;

    public Model_Book_Details() {
    }

    public Model_Book_Details(String isbn, String name, String writer, String edition, String status, int copies, String imageUri, String publisher, String categories, String description, boolean featured, String entry_date, String subject, String subtitle, String editor, String volume, String publication_place, String publication_date, String language, String pages, String dcc_number, String author_mark, String binding, int shelf_number, int shelf_row, double rating, String pdf) {
        this.isbn = isbn;
        this.name = name;
        this.writer = writer;
        this.edition = edition;
        this.status = status;
        this.copies = copies;
        ImageUri = imageUri;
        this.publisher = publisher;
        this.categories = categories;
        this.description = description;
        this.featured = featured;
        this.entry_date = entry_date;
        this.subject = subject;
        this.subtitle = subtitle;
        this.editor = editor;
        this.volume = volume;
        this.publication_place = publication_place;
        this.publication_date = publication_date;
        this.language = language;
        this.pages = pages;
        this.dcc_number = dcc_number;
        this.author_mark = author_mark;
        this.binding = binding;
        this.shelf_number = shelf_number;
        this.shelf_row = shelf_row;
        this.rating = rating;
        this.pdf = pdf;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCopies() {
        return copies;
    }

    public void setCopies(int copies) {
        this.copies = copies;
    }

    public String getImageUri() {
        return ImageUri;
    }

    public void setImageUri(String imageUri) {
        ImageUri = imageUri;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public String getEntry_date() {
        return entry_date;
    }

    public void setEntry_date(String entry_date) {
        this.entry_date = entry_date;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getDcc_number() {
        return dcc_number;
    }

    public void setDcc_number(String dcc_number) {
        this.dcc_number = dcc_number;
    }

    public String getAuthor_mark() {
        return author_mark;
    }

    public void setAuthor_mark(String author_mark) {
        this.author_mark = author_mark;
    }

    public String getPublication_place() {
        return publication_place;
    }

    public void setPublication_place(String publication_place) {
        this.publication_place = publication_place;
    }

    public String getPublication_date() {
        return publication_date;
    }

    public void setPublication_date(String publication_date) {
        this.publication_date = publication_date;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public String getBinding() {
        return binding;
    }

    public void setBinding(String binding) {
        this.binding = binding;
    }

    public int getShelf_number() {
        return shelf_number;
    }

    public void setShelf_number(int shelf_number) {
        this.shelf_number = shelf_number;
    }

    public int getShelf_row() {
        return shelf_row;
    }

    public void setShelf_row(int shelf_row) {
        this.shelf_row = shelf_row;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }
}



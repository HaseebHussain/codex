package com.example.codex;

import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import androidx.annotation.NonNull;

public class Custom_Methods {



    public boolean updateDeviceInstance(String ID){
        final boolean[] update = {false};
        final DatabaseReference query1 = FirebaseDatabase.getInstance().getReference("registered_users").child("students").child(ID);
        query1.child("device_instance").setValue(FirebaseInstanceId.getInstance().getToken())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                      update[0] = true;
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                update[0] = false;
            }
        });

        return update[0];
    }


}

package com.example.codex;


import android.content.Intent;

import android.os.Bundle;
import android.util.Log;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import android.widget.ListView;
import android.widget.Toast;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class Admin_Inqueries extends AppCompatActivity {
    private List<String> Inqueries;
    DatabaseReference myRef;
    ListView listInquiries;
    String[] array_id;
    Adapter_Users_Inquery_List adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin__inqueries);

        Inqueries = new ArrayList<>();
        listInquiries = findViewById(R.id.list_inquery);
        myRef = FirebaseDatabase.getInstance().getReference("Inquiries");
        array_id = new String[1];

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                Inqueries.clear();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Model_Inquery inquery = postSnapshot.getValue(Model_Inquery.class);
                    String student_id = inquery.getChat_id();

                    if(!Inqueries.contains(student_id)){
                        Inqueries.add(student_id);
                    }


                }

                adapter = new Adapter_Users_Inquery_List(Inqueries,Admin_Inqueries.this);
                listInquiries.setAdapter(adapter);


            }

            @Override
            public void onCancelled(@NotNull DatabaseError databaseError) {

            }
        });



    }
}

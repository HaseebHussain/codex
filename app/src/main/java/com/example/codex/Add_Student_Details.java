package com.example.codex;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import androidx.appcompat.app.AppCompatActivity;


public class Add_Student_Details extends AppCompatActivity {

    TextView med_studentid,med_name,med_year,med_address,med_contact;
    Spinner msp_department;
    Button mbtn_adduser;
    ProgressBar mprocess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__student__details);

        //Calling View Items Method
        CallViewItems();

        //Department Spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.department, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        msp_department.setAdapter(adapter);
        //End Department Spinner

        //Add new user Button Click Listener
        mbtn_adduser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AddStudent();

                Toast.makeText(Add_Student_Details.this, "New User Added Successfully", Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Write a message to the database
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Student_Details");

    public void CallViewItems(){
        med_studentid = findViewById(R.id.ed_studentid);
        med_name = findViewById(R.id.ed_name);
        med_year = findViewById(R.id.ed_year);
        med_address = findViewById(R.id.ed_address);
        msp_department = (Spinner) findViewById(R.id.sp_department);
        med_contact = findViewById(R.id.ed_contact);
        mprocess = findViewById(R.id.progress);
        mbtn_adduser = findViewById(R.id.btn_adduser);
    }

    //Add User Details
    public  void AddStudent(){
        mprocess.setVisibility(View.VISIBLE);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Student_Details");

        String id = myRef.push().getKey();
        String studentId = med_studentid.getText().toString();
        String studentname = med_name.getText().toString();
        String acedmicyear = med_year.getText().toString();
        String contact = med_contact.getText().toString();
        String adderss = med_address.getText().toString();
        String department = msp_department.getSelectedItem().toString();

        if(TextUtils.isEmpty(studentId) && TextUtils.isEmpty(studentname)
                && TextUtils.isEmpty(acedmicyear) && TextUtils.isEmpty(contact)
                && TextUtils.isEmpty(adderss)
        ){
            Toast.makeText(this, "Alert! Please insert all the details", Toast.LENGTH_SHORT).show();
        }
        else{
            Model_Student_Details newStudent = new Model_Student_Details(studentId,studentname,acedmicyear,contact,adderss,department,"");

            myRef.child(id).setValue(newStudent);
            mprocess.setVisibility(View.GONE);
        }

        med_studentid.setText("");
        med_name.setText("");
        med_year.setText("");
        med_contact.setText("");
        med_address.setText("");

    }

}






package com.example.codex;

public class Model_Student_Details {

    private String student_id;
    private String name;
    private String acadmic_year;
    private String contact;
    private String address;
    private String department;
    private String device_instance;


    public Model_Student_Details() {
    }

    public Model_Student_Details(String student_id, String name, String acadmic_year, String contact, String address, String department, String device_instance) {
        this.student_id = student_id;
        this.name = name;
        this.acadmic_year = acadmic_year;
        this.contact = contact;
        this.address = address;
        this.department = department;
        this.device_instance = device_instance;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAcadmic_year() {
        return acadmic_year;
    }

    public void setAcadmic_year(String acadmic_year) {
        this.acadmic_year = acadmic_year;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDevice_instance() {
        return device_instance;
    }

    public void setDevice_instance(String device_instance) {
        this.device_instance = device_instance;
    }
}


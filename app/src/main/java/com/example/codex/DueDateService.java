package com.example.codex;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import kotlin.random.Random;

import static android.app.Notification.DEFAULT_SOUND;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class DueDateService extends IntentService {

    SharedPreferences preferences;

    public DueDateService() {
        super("DueDateService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            preferences = getSharedPreferences("codex",MODE_PRIVATE);
            String student_id = preferences.getString("student_id","null");

            if(!student_id.equals("null")){
                DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("issued_books");
                Query q = myRef.orderByChild("student_id").equalTo(student_id);
                q.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            for(final DataSnapshot d : dataSnapshot.getChildren()){
                                try{
                                    Model_Book_Issue issue = d.getValue(Model_Book_Issue.class);
                                    ConnectionCheck check = new ConnectionCheck(getBaseContext());
                                    final String currentDate = check.CurrentDate();
                                    String dueDate = issue.getReturn_date();
                                    final int overdays = check.getDateReference(currentDate,dueDate);
                                    if(overdays == 0 && issue.getStatus().equals("issued")){
                                        DatabaseReference bookRef = FirebaseDatabase.getInstance().getReference("Book").child("-H"+issue.getBook_isbn());
                                        bookRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                if(dataSnapshot.exists()){
                                                        String book_title = dataSnapshot.child("name").getValue(String.class);
                                                        showNotification("Codex","The Book '"+book_title +"' needs to be returned on "+currentDate);

                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });

                                    }else if(overdays == -1 && issue.getStatus().equals("issued")){
                                        DatabaseReference bookRef = FirebaseDatabase.getInstance().getReference("Book").child("-H"+issue.getBook_isbn());
                                        bookRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                if(dataSnapshot.exists()){
                                                        String book_title = dataSnapshot.child("name").getValue(String.class);
                                                        showNotification("Codex","The Book '"+book_title +"' needs to be returned on "+currentDate);

                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });
                                    }else if(overdays > 0 && issue.getStatus().equals("issued")){
                                        DatabaseReference bookRef = FirebaseDatabase.getInstance().getReference("Book").child("-H"+issue.getBook_isbn());
                                        bookRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                if(dataSnapshot.exists()){
                                                        String book_title = dataSnapshot.child("name").getValue(String.class);
                                                        showNotification("Codex","The Book '"+book_title +"' due date is passed by  "+overdays+" days!");

                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });
                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }



                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

        }
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Codex";
            String description = "codex notify";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("1", name, importance);

            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void showNotification(String title, String body){
        createNotificationChannel();
        int num = (int) System.currentTimeMillis();

        @SuppressLint("WrongConstant") PendingIntent resultPendingIntent=PendingIntent.getActivity(this,num,new Intent(this,UserPanel.class),Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        long[] vibrate = { 0, 100, 200, 300 };
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "1")
                .setSmallIcon(R.drawable.codex)
                .setContentTitle(title)
                .setContentText(body)
                    .setSound(alarmSound)
                .setVibrate(vibrate)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

// notificationId is a unique int for each notification that you must define
        notificationManager.notify(num, builder.build());
    }

}

package com.example.codex;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


public class AdaptorUserInqueryMessages extends ArrayAdapter<Model_Inquery> {

    Context context;
    ArrayList<Model_Inquery> list;

    private static class ViewHolder {
        TextView txtTime;
        TextView txtMessage;
        TextView txtName;
    }

    public AdaptorUserInqueryMessages(Context context1, ArrayList<Model_Inquery> list) {
        super(context1, R.layout.layout_user_inquries_messages, list);
        this.context = context1;
        this.list = list;
    }

    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final Model_Inquery m = list.get(position);

        final ViewHolder viewHolder;


        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);

            String name = m.getSender();
            if (name.equals("Codex")) {

                convertView = inflater.inflate(R.layout.layout_admin_inquiry_msg, null, false);
                viewHolder.txtMessage = convertView.findViewById(R.id.txtMessage);
                viewHolder.txtTime = convertView.findViewById(R.id.txtTime);
                viewHolder.txtName = convertView.findViewById(R.id.txtName);

                viewHolder.txtName.setText(m.getSender());
                viewHolder.txtMessage.setText(m.getMessage());
                viewHolder.txtTime.setText(m.getTime());

            } else {


                convertView = inflater.inflate(R.layout.layout_user_inquries_messages, null, false);
                viewHolder.txtMessage = convertView.findViewById(R.id.txtMessage);
                viewHolder.txtTime = convertView.findViewById(R.id.txtTime);
                viewHolder.txtName = convertView.findViewById(R.id.txtName);

                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("registered_users").child("students");
                Query q = databaseReference.orderByChild("university_id").equalTo(m.getSender());

                q.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        String name = null;
                        for(DataSnapshot d : dataSnapshot.getChildren()){
                            name = d.child("name").getValue(String.class);


                        }
                        viewHolder.txtName.setText(name);
                        viewHolder.txtMessage.setText(m.getMessage());
                        viewHolder.txtTime.setText(m.getTime());

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }


            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();

        }



        return convertView;
    }
}

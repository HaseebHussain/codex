package com.example.codex;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.text.CollationElementIterator;
import java.util.ArrayList;
import java.util.List;

public class Activity_admin_issue_book extends AppCompatActivity {


    Toolbar mToolbar;
    ProgressBar progressBar;
    GridView gridView;
    Query mDbRef;
    Adapter_Book_Search mAdapter;
    List<Model_Book_Details> mBook_Details;
    String category;
    String user;
    TextView mTxtCategoryName;
    SharedPreferences preferences;
    FloatingActionButton mScanBook;
    String scanContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_issue_book);

        mToolbar = findViewById(R.id.search_toolbar);
        setSupportActionBar(mToolbar);
        mScanBook = findViewById(R.id.scan_book);
        mToolbar = findViewById(R.id.search_toolbar);
        setSupportActionBar(mToolbar);
        gridView = findViewById(R.id.Rv_books);
        preferences = getSharedPreferences("codex",MODE_PRIVATE);
        mBook_Details = new ArrayList<>();
        mDbRef = FirebaseDatabase.getInstance().getReference("Book");

        mScanBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(requsetPermissoin()){
                    IntentIntegrator scanIntegrator = new IntentIntegrator(Activity_admin_issue_book.this);
                    scanIntegrator.initiateScan();
                }
            }
        });

        mDbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                mBook_Details.clear();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    Model_Book_Details upload = postSnapshot.getValue(Model_Book_Details.class);

                    if(upload.getCopies() > 0){
                        mBook_Details.add(upload);
                    }

                }
//
                mAdapter = new Adapter_Book_Search(mBook_Details, Activity_admin_issue_book.this);
                gridView.invalidateViews();
                mAdapter.notifyDataSetChanged();
                gridView.setAdapter(mAdapter);

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        String user = preferences.getString("category","NOT FOUND");

                        Model_Book_Details pos = mBook_Details.get(position);

                            Intent intent = new Intent(Activity_admin_issue_book.this,admin_issue_book_detail.class);
                            intent.putExtra("isbn",pos.getIsbn());
                            startActivity(intent);


                    }
                });



            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {


            }
        });


        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.pullToRefresh);

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.notifyDataSetChanged();
                pullToRefresh.setRefreshing(false);
            }
        });



    }

    private boolean requsetPermissoin(){
        if (ActivityCompat.checkSelfPermission(Activity_admin_issue_book.this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Permission Already Granted", Toast.LENGTH_SHORT).show();
            return true;
        }
        else{
            ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.CAMERA},1);
            return false;
        }
        // Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //startActivityForResult(intent,0);


    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//retrieve scan result
        super.onActivityResult(requestCode, resultCode, intent);
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);

        if (scanningResult != null) {


//we have a result
            scanContent = scanningResult.getContents();
            //get format name of data scanned

            if (isNumeric(scanContent)) {
                Intent i = new Intent(Activity_admin_issue_book.this,admin_issue_book_detail.class);
                i.putExtra("isbn",scanContent);
                startActivity(i);

            } else {
                Toast.makeText(this, "ISBN number is not found", Toast.LENGTH_SHORT).show();
            }


        } else {
            Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT).show();
        }

    }

    private boolean isNumeric(String scanContent) {

        //Checking the qrcode is a isbn number not a link

        try {
            double d = Double.parseDouble(scanContent);
            return true;
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.student_home_menu,menu);
        MenuItem search= menu.findItem(R.id.app_bar_search);
        search.expandActionView();

        SearchView searchView = (SearchView) search.getActionView();
        searchView.setQueryHint("Type here to search books");
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        search.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
//                Fragment fragment = null;
//                Class FragClass = Fragment_Search_Books.class;
//                try {
//                    fragment = (Fragment) FragClass.newInstance();
//                    FragmentManager manager = getSupportFragmentManager();
//                    manager.beginTransaction().replace(R.id.fragment_user_panel,fragment).commit();
//
//
//
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                } catch (InstantiationException e) {
//                    e.printStackTrace();
//                }
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {

//                    Intent intent = new Intent(Activity_admin_issue_book.this,admin_book_issue_search.class);
//                    intent.putExtra("user","student");
//                    startActivity(intent);
//                    overridePendingTransition(R.anim.fadein, R.anim.fade_out);

                return true;
            }
        });
        searchView.setQueryHint("Type here to search books");
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {


                Toast.makeText(Activity_admin_issue_book.this, query, Toast.LENGTH_SHORT).show();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return true;
            }
        });


        return true;
    }
}

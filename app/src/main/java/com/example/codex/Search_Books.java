package com.example.codex;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Search_Books extends AppCompatActivity {


    Toolbar mToolbar;
    ProgressBar progressBar;
    GridView gridView;
    Query mDbRef;
    Adapter_Book_Search mAdapter;
    List<Model_Book_Details> mBook_Details;
    String category;
    String user;
    TextView mTxtCategoryName;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search__books);

        mToolbar = findViewById(R.id.search_toolbar);
        setSupportActionBar(mToolbar);
        gridView = findViewById(R.id.Rv_books);
        preferences = getSharedPreferences("codex",MODE_PRIVATE);
        mBook_Details = new ArrayList<>();
        mDbRef = FirebaseDatabase.getInstance().getReference("Book");

        progressBar = new ProgressBar(this);
        progressBar.setVisibility(View.VISIBLE);
        mDbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                mBook_Details.clear();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    String user = preferences.getString("category","NOT FOUND");

                    Model_Book_Details upload = postSnapshot.getValue(Model_Book_Details.class);
                    if(user == "admin"){
                        mBook_Details.add(upload);
                    }else{
                        if(upload.getCopies() > 0){
                            mBook_Details.add(upload);
                        }
                    }


                }
//

                mAdapter = new Adapter_Book_Search(mBook_Details, Search_Books.this);
                gridView.invalidateViews();
                mAdapter.notifyDataSetChanged();
                gridView.setAdapter(mAdapter);
                progressBar.setVisibility(View.GONE);
                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        String user = preferences.getString("category","NOT FOUND");

                        Model_Book_Details pos = mBook_Details.get(position);

                        if(user.equals("admin")){
                            Intent intent = new Intent(Search_Books.this,admin_book_edit.class);
                            intent.putExtra("isbn",pos.getIsbn());
                            startActivity(intent);
                        }else{
                            Intent intent = new Intent(Search_Books.this,bookDetails.class);
                            intent.putExtra("isbn",pos.getIsbn());
                            startActivity(intent);
                        }

                    }
                });



            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {


            }
        });


        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.pullToRefresh);

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.notifyDataSetChanged();
                pullToRefresh.setRefreshing(false);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.student_home_menu,menu);
        MenuItem search= menu.findItem(R.id.app_bar_search);
        search.expandActionView();
        search.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
//                Fragment fragment = null;
//                Class FragClass = Fragment_Search_Books.class;
//                try {
//                    fragment = (Fragment) FragClass.newInstance();
//                    FragmentManager manager = getSupportFragmentManager();
//                    manager.beginTransaction().replace(R.id.fragment_user_panel,fragment).commit();
//
//
//
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                } catch (InstantiationException e) {
//                    e.printStackTrace();
//                }
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {

                String user_category  = preferences.getString("category","null");


                if(user_category.equals("admin")){
                    Intent intent = new Intent(Search_Books.this,Admin_Panel.class);
                    intent.putExtra("user","student");
                    startActivity(intent);
                    overridePendingTransition(R.anim.fadein, R.anim.fade_out);
                }else{
                    Intent intent = new Intent(Search_Books.this,UserPanel.class);
                    intent.putExtra("user","student");
                    startActivity(intent);
                    overridePendingTransition(R.anim.fadein, R.anim.fade_out);
                }




                return true;
            }
        });

        SearchView searchView = (SearchView) search.getActionView();
        searchView.setQueryHint("Type here to search books");
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {


                Toast.makeText(Search_Books.this, query, Toast.LENGTH_SHORT).show();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return true;
            }
        });



        return true;
    }



}

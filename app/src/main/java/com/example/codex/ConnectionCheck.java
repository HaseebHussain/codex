package com.example.codex;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("ALL")
public class ConnectionCheck {

    Context mContext;

    public ConnectionCheck(Context mContext) {
        this.mContext = mContext;
    }

    public boolean RequestInternetConnection(){

        ConnectivityManager check = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        //noinspection deprecation
        NetworkInfo[] info = new NetworkInfo[0];
        if (check != null) {
            info = check.getAllNetworkInfo();
        }

        for (int i = 0; i<info.length; i++){
            if (info[i].getState() == NetworkInfo.State.CONNECTED){

                return true;
            }
        }

        return false;
    }


    public String CurrentDateTime(){
        DateFormat df = new SimpleDateFormat("dd/MMM h:mm aa");
        Date date = new Date();
        String formattedDate = df.format(date);

        return formattedDate;
    }

    public String CurrentDate(){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String formattedDate = df.format(date);

        return formattedDate;
    }

    public String CurrentTime(){
        DateFormat df = new SimpleDateFormat("h:mm aa");
        Date date = new Date();
        String formattedDate = df.format(date);

        return formattedDate;
    }

    public int getDateReference(String currentDate,String dueDate){
        try {
            SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
//        String inputString1 = "23 04 1997";
//        String inputString2 = "27 04 1997";
            String inputString1 = currentDate.replace("/"," ");
            String inputString2 = dueDate.replace("/"," ");
            Date date1 = myFormat.parse(inputString1);
            Date date2 = myFormat.parse(inputString2);
            long diff = date1.getTime()- date2.getTime();
            System.out.println ("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            return (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public String CalculateDays(String currentDate,String returnDate){
        SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
        String inputString1 = currentDate.replace("/"," ");
        String inputString2 = returnDate.replace("/"," ");

        try {
            Date date1 = myFormat.parse(inputString1);
            Date date2 = myFormat.parse(inputString2);
            long diff = date1.getTime() - date2.getTime();
            long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            int month = 0, year = 0, week = 0, day = 0;
//            for (int i = 1; i < days + 1; i++) {
//                if ((i % 30) == 0)
//                    month++;
//                if ((i % 7) == 0)
//                    week++;
//                if ((i % 365) == 0)
//                    ++year;
//                if ((i % 1) == 0)
//                    day++;
//
//            }
//
//                if(week <= 0){
//                    return day+" Days";
//                }else if(month <= 0){
//                    return week+" Week(s) "+((int)days-month)+" Days" ;
//                }else if(year <= 0){
//                    return month+" Month(s) "+((int)days-month)+" Days" ;
//                }else{
//                    return year+" Years "+(day-year)+" "
//                }

            year = (int)days/365;
            day=(int)days%365;

            month = (int)days/30;

            week = (int)days/7;


            if(week <= 0){
                return days+" Day(s)";
            }else if(month <= 0){
                int _days = (int)(days-(week*7));
                return week+" Week(s) "+_days+" Day(s)";
            }else if(year <= 0){


                    int remaining = (int) days;
                   remaining = (int)(remaining-(month*31));
                   int _weeks = (int)(remaining/7);
                   return month+" Month(s) "+_weeks+" Week(s)";
            }else{
                return year+" Year (s)";
            }

        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("Error "+e.toString());
            return  "";
        }

    }
}

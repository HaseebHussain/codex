package com.example.codex;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class Adaptor_Book_lists  extends BaseAdapter {

    private List<Model_Book_Details> book_list;
    private Context mContext;

    public Adaptor_Book_lists(List<Model_Book_Details> book_list, Context mContext) {
        this.book_list = book_list;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return book_list.size();
    }

    @Override
    public Object getItem(int position) {
        return book_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView;

        Model_Book_Details book_details = book_list.get(position);

        if (convertView == null) {

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.layout_books, null);

            // set image based on selected text
            ImageView imageView = gridView
                    .findViewById(R.id.img_book);

            TextView book_name = gridView.findViewById(R.id.tv_title);
            TextView book_author = gridView.findViewById(R.id.tv_writer);

            book_name.setText(book_details.getName());
            book_author.setText(book_details.getWriter());

            book_name.setSelected(true);

            Picasso.get()
                    .load(book_details.getImageUri())
                    .into(imageView);


        } else {
            gridView = convertView;
        }

        return gridView;
    }
}

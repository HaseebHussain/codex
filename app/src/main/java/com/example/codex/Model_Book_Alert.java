package com.example.codex;

public class Model_Book_Alert {
    private String alert_id;
    private String student_id;
    private String book_isbn;
    private String book_title;
    private String device_instance;

    public Model_Book_Alert() {
    }

    public Model_Book_Alert(String alert_id, String student_id, String book_isbn, String book_title, String device_instance) {
        this.alert_id = alert_id;
        this.student_id = student_id;
        this.book_isbn = book_isbn;
        this.book_title = book_title;
        this.device_instance = device_instance;
    }

    public String getAlert_id() {
        return alert_id;
    }

    public String getStudent_id() {
        return student_id;
    }

    public String getBook_isbn() {
        return book_isbn;
    }

    public String getBook_title() {
        return book_title;
    }

    public String getDevice_instance() {
        return device_instance;
    }
}

package com.example.codex;

import androidx.annotation.LongDef;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;

import java.util.ArrayList;


public class approved_books extends AppCompatActivity {

    private static final String TAG = "TAG";
    private RecyclerView recyclerView;
    private ArrayList<Model_Reserve_Book> mReserve_Books;
    private DatabaseReference mDatabaseRef;
    private Adaptor_Book_Approved mAdapter;
    private ProgressBar progressBar;
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approved_books);

        recyclerView = findViewById(R.id.bookApproved);

        mReserve_Books = new ArrayList<>();

        mDatabaseRef = FirebaseDatabase.getInstance().getReference("Reservations");
        progressBar = findViewById(R.id.process);


        ConnectionCheck cc = new ConnectionCheck(this);

        boolean connection = cc.RequestInternetConnection();
        progressBar = new ProgressBar(this);
        progressBar.setVisibility(View.VISIBLE);
        sharedpreferences = getSharedPreferences("approved", Context.MODE_PRIVATE);
        if (connection) {

            mDatabaseRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    mReserve_Books.clear();

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        String status = postSnapshot.child("status").getValue(String.class);
                        Log.i(TAG, "onDataChange: "+status);
                        if(status.equals("approved")){
                            Model_Reserve_Book book = postSnapshot.getValue(Model_Reserve_Book.class);
                            mReserve_Books.add(book);
                        }
                    }
                    progressBar.setVisibility(View.GONE);
                    mAdapter = new Adaptor_Book_Approved(approved_books.this,mReserve_Books);
                    recyclerView.setAdapter(mAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(approved_books.this));
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }else{
            Toast.makeText(this, "Internet is not connected", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);
        }
    }
}

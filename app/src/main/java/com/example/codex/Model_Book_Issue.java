package com.example.codex;

public class Model_Book_Issue {

    private String issue_id,book_isbn,student_id,issue_date,return_date,issue_time,status;

    public Model_Book_Issue() {
    }

    public Model_Book_Issue(String issue_id, String book_isbn, String student_id, String issue_date, String return_date, String issue_time, String status) {
        this.issue_id = issue_id;
        this.book_isbn = book_isbn;
        this.student_id = student_id;
        this.issue_date = issue_date;
        this.return_date = return_date;
        this.issue_time = issue_time;
        this.status = status;
    }

    public String getIssue_id() {
        return issue_id;
    }

    public void setIssue_id(String issue_id) {
        this.issue_id = issue_id;
    }

    public String getBook_isbn() {
        return book_isbn;
    }

    public void setBook_isbn(String book_isbn) {
        this.book_isbn = book_isbn;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getIssue_date() {
        return issue_date;
    }

    public void setIssue_date(String issue_date) {
        this.issue_date = issue_date;
    }

    public String getReturn_date() {
        return return_date;
    }

    public void setReturn_date(String return_date) {
        this.return_date = return_date;
    }

    public String getIssue_time() {
        return issue_time;
    }

    public void setIssue_time(String issue_time) {
        this.issue_time = issue_time;
    }

    public String getStatus() {
        return status;
    }
}

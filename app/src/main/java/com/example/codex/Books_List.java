package com.example.codex;



import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


public class Books_List extends AppCompatActivity {

    ProgressBar progressBar;
    GridView gridView;
    Query mDbRef;
    Adaptor_Book_lists mAdapter;
    List<Model_Book_Details> mBook_Details;
    String category;
    String user;
    TextView mTxtCategoryName;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books__list);




        progressBar = findViewById(R.id.process);

        gridView = findViewById(R.id.Rv_books);
        mTxtCategoryName = findViewById(R.id.txtCategoryTitle);

        mBook_Details = new ArrayList<>();
        registerForContextMenu(gridView);
        category = getIntent().getStringExtra("category");
        mTxtCategoryName.setText(category);
        mDbRef = FirebaseDatabase.getInstance().getReference("Book").orderByChild("categories").equalTo(category);

        preferences = getSharedPreferences("codex",MODE_PRIVATE);


        progressBar.setVisibility(View.VISIBLE);
        mDbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                mBook_Details.clear();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    Model_Book_Details upload = postSnapshot.getValue(Model_Book_Details.class);

                        mBook_Details.add(upload);

                }
//
                progressBar.setVisibility(View.GONE);
                mAdapter = new Adaptor_Book_lists(mBook_Details, Books_List.this);
                gridView.setAdapter(mAdapter);

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        String user = preferences.getString("category","NOT FOUND");

                        Model_Book_Details pos = mBook_Details.get(position);

                        if(user.equals("admin")){
                            Intent intent = new Intent(Books_List.this,admin_book_edit.class);
                            intent.putExtra("isbn",pos.getIsbn());
                            startActivity(intent);
                        }else{
                            Intent intent = new Intent(Books_List.this,bookDetails.class);
                            intent.putExtra("isbn",pos.getIsbn());
                            startActivity(intent);
                        }

                    }
                });



            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {


            }
        });


        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.pullToRefresh);

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.notifyDataSetChanged();
                pullToRefresh.setRefreshing(false);
            }
        });

        user = getIntent().getExtras().getString("user");

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if(user.equals( "admin")){
            menu.setHeaderTitle("What would you like to do with this book?");
            menu.setHeaderIcon(R.drawable.codex);
            getMenuInflater().inflate(R.menu.book_options,menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.edit:
                Toast.makeText(this, "Edit is clicked", Toast.LENGTH_SHORT).show();

                return true;
            case R.id.delete:
                Toast.makeText(this, "Delete is clicked", Toast.LENGTH_SHORT).show();


//                Model_Book_Details pos = (Model_Book_Details) gridView.getSelectedItem();
//                String isbn = pos.getIsbn();
//                show_Dailog(isbn);
                return true;
            default:
                return super.onContextItemSelected(item);
        }

    }


}



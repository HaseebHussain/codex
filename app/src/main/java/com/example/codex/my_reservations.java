package com.example.codex;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class my_reservations extends AppCompatActivity {

    DatabaseReference myRef;
    String student_id;
    ArrayList<Model_Reserve_Book> mList;
    RecyclerView recyclerView;
    SharedPreferences preferences;
    Toolbar mToolbar;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_reservations);

        recyclerView = findViewById(R.id.rv_my_reservations);
        mList = new ArrayList<>();
        preferences = getSharedPreferences("codex",MODE_PRIVATE);
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitle("My Book Reservations");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressBar = findViewById(R.id.waiting);


        myRef = FirebaseDatabase.getInstance().getReference("Reservations");
        student_id = preferences.getString("student_id","Null");

        myRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                mList.clear();
                for(DataSnapshot d: dataSnapshot.getChildren()){

                    String student_Id = d.child("student_id").getValue(String.class);

                    if(student_Id.equals(student_id)){
                        Model_Reserve_Book m = d.getValue(Model_Reserve_Book.class);
                        mList.add(m);

                    }

                }

                Adapter_My_Reservations rev = new Adapter_My_Reservations(my_reservations.this,mList);
                recyclerView.setAdapter(rev);


                LinearLayoutManager layoutManager = new
                        LinearLayoutManager(my_reservations.this, LinearLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(layoutManager);
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }
}

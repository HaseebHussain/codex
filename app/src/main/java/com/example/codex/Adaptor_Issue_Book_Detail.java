package com.example.codex;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Adaptor_Issue_Book_Detail extends BaseAdapter implements Filterable {

    private List<Model_Book_Details> book_list;
    private List<Model_Book_Details> book_list_Full;
    private Context mContext;

    public Adaptor_Issue_Book_Detail() {
    }

    public Adaptor_Issue_Book_Detail(List<Model_Book_Details> book_list, List<Model_Book_Details> book_list_Full, Context mContext) {
        this.book_list = book_list;
        this.book_list_Full = book_list_Full;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {

            return book_list.size();
    }

    @Override
    public Object getItem(int position) {

            return book_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Model_Book_Details book_details = book_list.get(position);

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.layout_books,null);
            // get layout from mobile.xml
        }
        convertView = inflater.inflate(R.layout.layout_books, null);

        // set image based on selected text
        ImageView imageView = convertView.findViewById(R.id.img_book);

        TextView book_name = convertView.findViewById(R.id.tv_title);
        TextView book_author = convertView.findViewById(R.id.tv_writer);

        book_name.setText(book_details.getName());
        book_author.setText(book_details.getWriter());

        book_name.setSelected(true);

        Picasso.get()
                .load(book_details.getImageUri())
                .into(imageView);




        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                book_list.clear();
                book_list.addAll((Collection<? extends Model_Book_Details>) results.values);
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                String query = constraint.toString().toLowerCase().trim();
                ArrayList<Model_Book_Details> FilteredList= new ArrayList<>();
                if (query.length() == 0 && query == "") {
                    // No filter implemented we return all the list
                    results.values = book_list_Full;
                }
                else {
                    for (int i = 0; i < book_list_Full.size(); i++) {
                        Model_Book_Details data = book_list_Full.get(i);
                        if (data.getName().toLowerCase().contains(query))  {
                            FilteredList.add(data);
                        }
//                        else if(data.getWriter().toLowerCase().contains(query)){
//                            FilteredList.add(data);
//                        }else if(data.getIsbn().toLowerCase().contains(query)){
//                            FilteredList.add(data);
//                        }
                    }
                    results.values = FilteredList;
                }
                return results;
            }
        };
    }
}

package com.example.codex;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;

public class Adapter_book_issue extends BaseAdapter {

    private List<Model_Book_Issue> mBookList;
    Context mContext;
    DatabaseReference bookRef,memberRef;
    ProgressDialog progressDialog;
    SharedPreferences preferences;

    public Adapter_book_issue(List<Model_Book_Issue> mBookList, Context mContext) {
        this.mBookList = mBookList;
        this.mContext = mContext;


    }

    @Override
    public int getCount() {
        return mBookList.size();
    }

    @Override
    public Object getItem(int position) {
        return mBookList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {
        final LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View gridView;

        final Model_Book_Issue book_details = mBookList.get(position);
        bookRef = FirebaseDatabase.getInstance().getReference("Book");
        memberRef = FirebaseDatabase.getInstance().getReference("registered_users").child("students");


        if (convertView == null) {

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.layout_books, null);
            final String isbn = "-H"+book_details.getBook_isbn();
            preferences = mContext.getSharedPreferences("codex",Context.MODE_PRIVATE);
            final String category = preferences.getString("category","null");
            Log.i("TAG", "getView: Category  = "+category);

               gridView.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                       final Dialog d = new Dialog(mContext);
                       d.setContentView(R.layout.layout_return_book);

                       final TextView mBookTitle = d.findViewById(R.id.book_title);
                       final TextView mMemberName = d.findViewById(R.id.member_name);
                       TextView mIssueDate = d.findViewById(R.id.issue_date);
                       final TextView mDueDate = d.findViewById(R.id.due_date);
                       MaterialButton mReturnBook = d.findViewById(R.id.returnBook);
                       MaterialButton mRenewBook = d.findViewById(R.id.renew_book);
                       final ImageView mBookImage = d.findViewById(R.id.book_image);
                       bookRef.child("-H" + book_details.getBook_isbn())
                               .addValueEventListener(new ValueEventListener() {
                                   @Override
                                   public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                       if (dataSnapshot.exists()) {
                                           Log.i("TAG", "onDataChange: " + dataSnapshot);

                                           String title = dataSnapshot.child("name").getValue(String.class);
                                           mBookTitle.setText(title);

                                           String book_image = dataSnapshot.child("imageUri").getValue(String.class);
                                           Picasso.get().load(book_image).into(mBookImage);
                                       }
                                   }

                                   @Override
                                   public void onCancelled(@NonNull DatabaseError databaseError) {

                                   }
                               });


                       memberRef.child(book_details.getStudent_id())
                               .addValueEventListener(new ValueEventListener() {
                                   @Override
                                   public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                       if (dataSnapshot.exists()) {
                                           String name = dataSnapshot.child("name").getValue(String.class);
                                           mMemberName.setText(name);
                                       }
                                   }

                                   @Override
                                   public void onCancelled(@NonNull DatabaseError databaseError) {

                                   }
                               });
                       mIssueDate.setText(book_details.getIssue_date());
                       mDueDate.setText(book_details.getReturn_date());
                       mRenewBook.setVisibility(View.GONE);
                       mReturnBook.setVisibility(View.GONE);
                       if (category.equals("admin")) {
                           mRenewBook.setVisibility(View.VISIBLE);
                           mReturnBook.setVisibility(View.VISIBLE);
                           mReturnBook.setOnClickListener(new View.OnClickListener() {
                               @Override
                               public void onClick(View v) {

                                   try {
                                       progressDialog = new ProgressDialog(mContext);
                                       progressDialog.setMessage("Please Wait...!");
                                       progressDialog.show();
                                       ConnectionCheck c = new ConnectionCheck(mContext);
                                       String currentDate = c.CurrentDate();
                                       DatabaseReference returnRef = FirebaseDatabase.getInstance().getReference("books_transactions");
                                       String return_id = returnRef.push().getKey();
                                       String dueDate = book_details.getReturn_date();
                                       final String issueID = book_details.getIssue_id();
                                       int overDueDays = c.getDateReference(currentDate, dueDate);
                                       if (overDueDays < 0) {
                                           overDueDays = 0;
                                       }
                                       Double fine = (double) (overDueDays * 5);
                                       Model_Book_Return aReturn = new Model_Book_Return(issueID, return_id, book_details.getStudent_id(), book_details.getBook_isbn(), currentDate, book_details.getReturn_date(), overDueDays, fine, true);

//                              setting return book ref
                                       returnRef.child(return_id).setValue(aReturn)
                                               .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                   @Override
                                                   public void onComplete(@NonNull Task<Void> task) {
                                                       DatabaseReference issueRef = FirebaseDatabase.getInstance().getReference("issued_books");
                                                       issueRef.child(issueID).removeValue()
                                                               .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                   @Override
                                                                   public void onComplete(@NonNull Task<Void> task) {
                                                                       final DatabaseReference bookRef = FirebaseDatabase.getInstance().getReference("Book");
                                                                       bookRef.child("-H" + book_details.getBook_isbn())
                                                                               .addListenerForSingleValueEvent(new ValueEventListener() {
                                                                                   @Override
                                                                                   public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                                       if (dataSnapshot.exists()) {
                                                                                           Log.i("TAG", "onDataChange: Book ISBN" + book_details.getBook_isbn());
                                                                                           int copies = dataSnapshot.child("copies").getValue(Integer.class);
                                                                                           Log.i("TAG", "onDataChange: Copies " + copies);
                                                                                           int copiesCount = copies + 1;
                                                                                           bookRef.child("-H" + book_details.getBook_isbn()).child("copies").setValue(copiesCount)
                                                                                                   .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                                       @Override
                                                                                                       public void onComplete(@NonNull Task<Void> task) {
                                                                                                           progressDialog.dismiss();

                                                                                                           Toast.makeText(mContext, "Book marked as returned...!", Toast.LENGTH_SHORT).show();
                                                                                                           d.dismiss();
                                                                                                       }
                                                                                                   });
                                                                                       }
                                                                                   }

                                                                                   @Override
                                                                                   public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                                   }
                                                                               });
                                                                   }
                                                               });
                                                   }
                                               });


                                   } catch (DatabaseException e) {
                                       Toast.makeText(mContext, e.toString(), Toast.LENGTH_SHORT).show();
                                   } catch (Exception e) {
                                       Toast.makeText(mContext, e.toString(), Toast.LENGTH_SHORT).show();
                                   }

                               }
                           });


                           //                                re new Book
                           mRenewBook.setOnClickListener(new View.OnClickListener() {
                               @Override
                               public void onClick(View v) {
                                   try {
                                       progressDialog = new ProgressDialog(mContext);
                                       progressDialog.setMessage("Please Wait...!");
                                       progressDialog.show();
                                       DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                                       Calendar cal = Calendar.getInstance();
                                       cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + 6);
                                       Date date = cal.getTime();
                                       String returnDate = df.format(date);

                                       String currentDate = new ConnectionCheck(mContext).CurrentDate();
                                       String currentTime = new ConnectionCheck(mContext).CurrentTime();
                                       DatabaseReference ref = FirebaseDatabase.getInstance().getReference("issued_books");
//                            Log.i("TAG", " Return Date : "+returnDate);
                                       String id = ref.push().getKey();
                                       Model_Book_Issue data = new Model_Book_Issue(id, book_details.getBook_isbn(), book_details.getStudent_id(), currentDate, returnDate, currentTime, "issued");
                                       ref.child(id).setValue(data).addOnCompleteListener(new OnCompleteListener<Void>() {
                                           @Override
                                           public void onComplete(@NonNull Task<Void> task) {
                                               ConnectionCheck c = new ConnectionCheck(mContext);
                                               String currentDate = c.CurrentDate();
                                               DatabaseReference returnRef = FirebaseDatabase.getInstance().getReference("books_transactions");
                                               String return_id = returnRef.push().getKey();
                                               String dueDate = book_details.getReturn_date();
                                               final String issueID = book_details.getIssue_id();
                                               int overDueDays = c.getDateReference(currentDate, dueDate);
                                               if (overDueDays < 0) {
                                                   overDueDays = 0;
                                               }
                                               Double fine = (double) (overDueDays * 5);
                                               Model_Book_Return aReturn = new Model_Book_Return(issueID, return_id, book_details.getStudent_id(), book_details.getBook_isbn(), currentDate, book_details.getReturn_date(), overDueDays, fine, true);
                                               returnRef.child(return_id).setValue(aReturn)
                                                       .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                           @Override
                                                           public void onComplete(@NonNull Task<Void> task) {
                                                               DatabaseReference issueRef = FirebaseDatabase.getInstance().getReference("issued_books");
                                                               issueRef.child(issueID).removeValue()
                                                                       .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                           @Override
                                                                           public void onComplete(@NonNull Task<Void> task) {
                                                                               progressDialog.dismiss();
                                                                               d.dismiss();
                                                                               Toast.makeText(mContext, "Book marked as issued.", Toast.LENGTH_SHORT).show();
                                                                           }
                                                                       });

                                                           }
                                                       });
                                           }
                                       });
                                   } catch (Exception e) {
                                       e.printStackTrace();
                                   }
                               }
                           });
                       }

                       d.show();
                   }
               });



            DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("Book").child(isbn);
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){


            // set image based on selected text
            ImageView imageView = gridView.findViewById(R.id.img_book);
            TextView book_name = gridView.findViewById(R.id.tv_title);
            TextView book_author = gridView.findViewById(R.id.tv_writer);

            book_name.setText(dataSnapshot.child("name").getValue(String.class));
            book_author.setText(dataSnapshot.child("writer").getValue(String.class));

            book_name.setSelected(true);
            book_author.setSelected(true);

            Picasso.get()
                    .load(dataSnapshot.child("imageUri").getValue(String.class))
                    .into(imageView);

                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });



        } else {
            gridView = convertView;
        }

        return gridView;
    }
}

package com.example.codex;

public class Model_Student_Detail {

    String student_id;
    String university_id;
    String registratin_date;
    String name;
    String father_name;
    String address;
    String phone_no;
    String email;
    String password;
    String department;
    String batch;
    String batch_year;
    String cnic;
    String gender;
    String picture;
    String device_instance;
    int book_issued;
    String status;
    boolean blocked;

    public Model_Student_Detail() {
    }

    public Model_Student_Detail(String student_id, String university_id, String registratin_date, String name, String father_name, String address, String phone_no, String email, String password, String department, String batch, String batch_year, String cnic, String gender, String picture, String device_instance, int book_issued, String status, boolean blocked) {
        this.student_id = student_id;
        this.university_id = university_id;
        this.registratin_date = registratin_date;
        this.name = name;
        this.father_name = father_name;
        this.address = address;
        this.phone_no = phone_no;
        this.email = email;
        this.password = password;
        this.department = department;
        this.batch = batch;
        this.batch_year = batch_year;
        this.cnic = cnic;
        this.gender = gender;
        this.picture = picture;
        this.device_instance = device_instance;
        this.book_issued = book_issued;
        this.status = status;
        this.blocked = blocked;
    }

    public String getStudent_id() {
        return student_id;
    }

    public String getUniversity_id() {
        return university_id;
    }

    public String getRegistratin_date() {
        return registratin_date;
    }

    public String getName() {
        return name;
    }

    public String getFather_name() {
        return father_name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getDepartment() {
        return department;
    }

    public String getBatch() {
        return batch;
    }

    public String getBatch_year() {
        return batch_year;
    }

    public String getCnic() {
        return cnic;
    }

    public String getGender() {
        return gender;
    }

    public String getPicture() {
        return picture;
    }

    public int getBook_issued() {
        return book_issued;
    }

    public String getStatus() {
        return status;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public String getDevice_instance() {
        return device_instance;
    }
}

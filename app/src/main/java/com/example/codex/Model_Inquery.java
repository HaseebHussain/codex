package com.example.codex;


public class Model_Inquery {
    private String message_id;
    private String sender;
    private String message;
    private String device_instance;
    private String time;
    private String receiver;
    private boolean read;
    private String chat_id;

    public Model_Inquery() {
    }


    public Model_Inquery(String message_id, String sender, String message, String device_instance, String time, String receiver, boolean read, String chat_id) {
        this.message_id = message_id;
        this.sender = sender;
        this.message = message;
        this.device_instance = device_instance;
        this.time = time;
        this.receiver = receiver;
        this.read = read;
        this.chat_id = chat_id;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDevice_instance() {
        return device_instance;
    }

    public void setDevice_instance(String device_instance) {
        this.device_instance = device_instance;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public String getChat_id() {
        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }
}

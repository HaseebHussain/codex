package com.example.codex;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ListAdapter;

public class Adapter_Users_Previous_Books extends BaseAdapter {

    private Context mContext;
    private List<Model_Book_Return> bookhistory;

    public Adapter_Users_Previous_Books(Context mContext, List<Model_Book_Return> bookhistory) {
        this.mContext = mContext;
        this.bookhistory = bookhistory;
    }

    @Override
    public int getCount() {
        return bookhistory.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_users_previous_books,null,false);

            final TextView mBookTitle = convertView.findViewById(R.id.book_title);
            final TextView mBookDate = convertView.findViewById(R.id.date);
            final ImageView image = convertView.findViewById(R.id.book_image);
            RelativeLayout mLayout = convertView.findViewById(R.id.layout);

            final Model_Book_Return book_return = bookhistory.get(position);
            String isbn = book_return.getBook_isbn();

            final Dialog dialog = new Dialog(mContext);
            dialog.setContentView(R.layout.layout_previous_book_details);
            dialog.setTitle("Details");

            final TextView d_title = dialog.findViewById(R.id.book_title);
            final ImageView d_image = dialog.findViewById(R.id.book_image);
            TextView d_fine = dialog.findViewById(R.id.fine);
            TextView d_overdays = dialog.findViewById(R.id.overdue_days);
            TextView d_due_date = dialog.findViewById(R.id.due_date);
            TextView d_return_date = dialog.findViewById(R.id.return_date);

            d_fine.setText(String.valueOf(book_return.getFine()));
            d_overdays.setText(String.valueOf(book_return.getOverdue_days()));
            d_due_date.setText(book_return.getDue_date());
            d_return_date.setText(book_return.getReturn_date());




            DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("Book");
            myRef.child("-H"+isbn).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        String book_title = dataSnapshot.child("name").getValue(String.class);
                        String img = dataSnapshot.child("imageUri").getValue(String.class);
                        d_title.setText(book_title);
                        mBookTitle.setText(book_title);
                        if(img != null && !img.equals("")){
                            Picasso.get().load(img).into(image);
                            Picasso.get().load(img).into(d_image);
                        }
                        //            Calculate the days
                        String days = new ConnectionCheck(mContext).CalculateDays(new ConnectionCheck(mContext).CurrentDate(),book_return.getReturn_date());
                        mBookDate.setText(days+" ago");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            mLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.show();
                }
            });
        }

        return convertView;
    }
}
